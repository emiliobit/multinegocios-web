<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Flash errors -->
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-<?= $__level ?> alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><?= html_escape('×') ?></button>
            <?php if (is_string($__message)): ?>
                <h1><span class="fa fa-<?= $__level == 'danger' ? 'ban' : $__level ?>"></span> <?= $__message ?></h1>
            <?php elseif (is_array($__message)): ?>
                <?php foreach ($__message as $__msg): ?>
                    <h1><span class="fa fa-<?= $__level == 'danger' ? 'ban' : $__level ?>"></span> <?= $__msg ?></h1><br>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
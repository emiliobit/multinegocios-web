<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row redondeado">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 centrado_formdom">
		<form action="<?= base_url('dominios/check-py') ?>" method="post" class="form-inline">
			<div class="form-group" >    
				<p class="form-control-static" style="margin:10px 0px 0px 0px">www.</p>
			</div>
			<div class="form-group" style="margin:2px 0px 0px 0px">    
				<input name="dominio" type="text" class="form-control" id="" placeholder="dominio">
			</div>
			<div class="form-group" style="margin:2px 0px 0px 0px">
				<select class="c-select" name="tld">
				
					<option selected value=".com.py">.com.py</option>            
					<option value=".edu.py">.edu.py</option>            
					<option value=".net.py">.net.py</option>           
					<option value=".org.py">.org.py</option>            
					<option value=".coop.py">.coop.py</option>            
					<option value=".gov.py">.gov.py</option>            
					<option value=".mil.py">.mil.py</option>		
					
				</select>
			</div>
			<button type="submit" class="btn btn-primary">Consultar</button>
		</form>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	<?=  $config->detalle_dominio_py ?>
	</div>
</div>
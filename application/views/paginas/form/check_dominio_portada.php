<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

	<form action="<?= base_url('dominios/check') ?>" method="post" class="form-inline">
		<div class="form-group" >    
			<p class="form-control-static" style="margin:10px 0px 0px 0px">www.</p>
		</div>
		<div class="form-group" style="margin:2px 0px 0px 0px">    
			<input name="dominio" type="text" class="form-control" id="" placeholder="dominio">
		</div>
		<div class="form-group" style="margin:2px 0px 0px 0px">
			<select class="c-select" name="tld">
				<option selected value=".com">.com</option>
				<option value=".net">.net</option>
				<option value=".org">.org</option>
				<option value=".info">.info</option>
				<option value=".biz">.biz</option>
				<option value=".es">.es</option>
			</select>
		</div>
		<button type="submit" class="btn btn-primary">Consultar</button>
	</form>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
<form action="<?= base_url('whois/registrar_privado') ?>" method="post" class="form-inline">

	<input type="hidden" name="solicitud_whois_priv" value="solicitud_whois_priv">
	<input type="hidden" name="sitio_email" value="<?= $config->email_sitio; ?>">

    <div style="text-align:center; min-height:150px;">
	<div class="form-group" >    
        <p class="form-control-static" style="margin:10px 0px 0px 0px">www.</p>
    </div>
    <div class="form-group" style="margin:2px 0px 0px 0px">    
        <input name="dominio" type="text" class="form-control" id="" placeholder="dominio">
    </div>
    <div class="form-group" style="margin:2px 0px 0px 0px">
        <select class="c-select" name="tld">
            <option selected value=".com">.com</option>
            <option value=".net">.net</option>
            <option value=".org">.org</option>
            <option value=".info">.info</option>
            <option value=".biz">.biz</option>
            <option value=".es">.es</option>
        </select>
    </div>
	</div>
<!-- Formulario
==================== -->	
	<div class="form-inline row">																
		<h4>Complete el formulario con sus datos</h4>
		
		<?php if (isset($error_envio) && $error_envio === TRUE): ?>
				<p>Ocurrio un error al intentar enviar los datos, por favor intente de nuevo</p>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong><?= validation_errors() ?></strong> 
				</div>
        <?php endif; ?>
		
		<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<span class="formInputName">Nombre:</span>
			<input name="nombre" type="text" placeholder="Nombre"  class="form-control" value="<?= set_value('nombre') ?>" required>
		</div>
		<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<span class="formInputName">Apellido:</span>
			<input name="apellido" type="text" placeholder="Apellido"  class="form-control" value="<?= set_value('apellido') ?>" required>
		</div>
	</div>
	   
	<div class="form-inline row">
		<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<span class="formInputName">Email:</span>
			<input name="email" type="email" placeholder="Correo Electrónico"  class="form-control" value="<?= set_value('email') ?>" required>
		</div>
		<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<span class="formInputName">Telefono:</span>
			<input name="telefono" type="tel" placeholder="Telefono" class="form-control" value="<?= set_value('telefono') ?>" required>
		</div>
	</div>
	
	
    <button type="submit" class="btn btn-primary">Enviar</button>
</form>
</div>
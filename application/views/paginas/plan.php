<section class="mainContent">
	<div class="container">  
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 data-wow-duration="500ms" data-wow-delay=".2s" class="mex-title wow fadeInLeft animated" style="visibility: visible; animation-duration: 500ms; animation-delay: 0.2s; animation-name: fadeInUp;">  			<h1 class="subtitle" style="text-align:center; padding-top:20px"><?= $plan_dato->nombre_plan ?></h1>	
			<div class="base-icone" style="text-align:center;"><span class="icon"><i style="font-size:100px;" class="icomoon-cloud-upload"></i></span></div>
			<div data-progress="90" class="progress  progress-striped progress-blue">
				<div class="bar" style="width: 150px;"><?= $plan_dato->espacio ?></div>
			</div>				 
			<h1 style="text-align:center"><?= $config->simbolo_moneda.NumFormat($plan_dato->costo) ?></h1>
		</div>
		<div class="padre col-lg-4 col-md-4 col-sm-4 c-plan-d data-wow-duration="500ms" data-wow-delay=".2s" class="mex-title wow fadeInLeft animated" style="visibility: visible; animation-duration: 500ms; animation-delay: 0.2s; animation-name: fadeInUp;">
			<div class="hijo"><?= $plan_dato->descripcion_corta ?></div>
		</div>
		<div class="padre col-lg-4 col-md-4 col-sm-4 data-wow-duration="500ms" data-wow-delay=".2s" class="mex-title wow fadeInLeft animated" style="visibility: visible; animation-duration: 500ms; animation-delay: 0.2s; animation-name: fadeInUp;">
			  <div class="hijo" style="text-align:center;">			  
			  <div><a class=" btn btn-primary btn-lg" href="<?php echo base_url(); ?>plan/solicitar/<?php echo $plan_dato->id_planes; ?>">Crear Cuenta</a></div>
			  </div>
		</div>
		</div>
	</div>
</section>

<section class="mainContent">
<hr class="vertical-space2">
	<div class="container">  
		<div class="row" data-wow-duration="500ms" data-wow-delay=".2s" class="mex-title wow fadeInLeft animated" style="visibility: visible; animation-duration: 500ms; animation-delay: 0.2s; animation-name: fadeInLeft;" >
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
				<?= $plan_dato->descripcion_detalle ?>				
			</div>
			
		</div>
	</div>
</section>





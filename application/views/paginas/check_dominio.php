<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="mainContent full-width">
  
<div class="container">  
	<div class="row">  
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sectionTitle">          
			<hr class="vertical-space3">
			<h2> Resultado de la consulta </h2>          
			<hr class="vertical-space2">
        </div>          
	</div>

<?php if (isset($check)): // si se hizo un checkeo ?>
    <?php if ($check['status'] === TRUE): ?>
        <?php if ($check['available'] === TRUE): ?>
		
		
		 <div class="row">  
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="thumbnail ">           
            <div class="caption text-center">
           <div class="captionImage"><i class="icomoon-thumbs-up-2 right" style="font-size:100px; color:green"></i></div>
           <h3>DISPONIBLE!!!</h3>
            <p><h4>El dominio: www.<?= html_escape($dominio) ?></h4></p>
            <form action="<?= base_url('dominios/registrar') ?>" method="post">
                <input type="hidden" name="dominio" value="<?= $dominio ?>"> 
                <button>Registrar</button>
            </form>
            </div>
			</div>
          </div> 
      </div>		
			
        <?php else: ?>
		
		
		<div class="row">  
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="thumbnail ">           
            <div class="caption text-center">
		<div class="captionImage"><i class="icomoon-thumbs-down-2 right" style="font-size:100px; color:red"></i></div>
            <p><h4>El dominio: www.<?= html_escape($dominio) ?></h4></p>
			<h3>No Esta disponible!!</h3>
            <?= $this->load->view('paginas/form/check_dominio', NULL, TRUE) ?>
			
			</div>
          </div> 
      </div>
	  </div>
        <?php endif; ?>
    <?php else: ?>
        <p><?= $check['message'] ?></p>
        <?= $this->load->view('paginas/form/check_dominio', NULL, TRUE) ?>
    <?php endif; ?>
<?php else: ?> 
    <?= $this->load->view('paginas/form/check_dominio', NULL, TRUE) ?>
<?php endif ?>

</div>
  <hr class="vertical-space1">
</section>

<section class="mainContent full-width">  
	<div class="container"> 
		<div class="row">  
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?= $config->detalle_dominio;?>
			</div>
		</div>
	</div>
</section>



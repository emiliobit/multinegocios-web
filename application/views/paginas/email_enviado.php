<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
	<div class="container">
		<div class="row">  
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="thumbnail ">           
					<div class="caption text-center" style="min-height:300px">
					   <div class="captionImage"><i class="icomoon-thumbs-up-2 right" style="font-size:100px; color:green"></i></div>
					   <h3>Su Mensaje fue enviado con Exito!</h3>					
					</div>
				</div>
			  </div> 
		  </div>

	</div>
</section>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- H1 Header starts
  ================================================== -->
<section class="h1Header">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sectionTitle wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="500ms">
        <div class="container">
            <h2>Registrar dominio</h2>
            <h2>www.<?= html_escape($dominio) ?></h2>
            <!--
                <ul class="breadcrumbs">
                 <li><a href="index-2.html">Home</a> </li>
                <li><a href="contact.html">Contact</a></li>
                </ul>
            -->
        </div>
    </div>
</section>

<!-- H1 Header ends
  ================================================== -->

<!-- contact Start
  ================================================== -->
<section class="mainContent contactDetails">
    <div class="container">
        <hr class="vertical-space1">
        <div class="row">
            <!-- Contact form  start
              ==================== -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contactForm wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">

                <h4>Complete el formulario con sus datos</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">                        
						
                        <?php if (isset($error_envio) && $error_envio === TRUE): ?>
						<div class="alert alert-error">
						<p>Ocurrio un error al intentar enviar los datos, por favor intente de nuevo</p>
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong><?= validation_errors() ?></strong>							
						</div>
                        
                        <?php endif; ?>
                        <form action="<?= base_url('dominios/registrar') ?>" method="post" role="form">
						
                            <input type="hidden" name="dominio" value="<?= $dominio ?>">
							<input type="hidden" name="sitio_email" value="<?= $config->email_sitio; ?>">
                            <input type="hidden" name="reg_form" value="reg_form">
							
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Nombre:</span>
                                    <input name="nombre" type="text" placeholder="Nombre y Apellido"  class="form-control" value="<?= set_value('nombre') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Apellido:</span>
                                    <input name="apellido" type="text" placeholder="Apellido"  class="form-control" value="<?= set_value('apellido') ?>" required>
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Empresa:</span>
                                    <input name="empresa" type="text" placeholder="Empresa (opcional)"  class="form-control" value="<?= set_value('empresa') ?>">
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Fax:</span>
                                    <input name="fax" type="tel" placeholder="Fax (opcional)" class="form-control" value="<?= set_value('fax') ?>">
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">CI:</span>
                                    <input name="cedula" type="text" placeholder="Cedula de identidad" class="form-control" value="<?= set_value('cedula') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">RUC:</span>
                                    <input name="ruc" type="text" placeholder="RUC (opcional)" class="form-control" value="<?= set_value('ruc') ?>">
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Email:</span>
                                    <input name="email" type="email" placeholder="Correo Electrónico"  class="form-control" value="<?= set_value('email') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Telefono:</span>
                                    <input name="telefono" type="tel" placeholder="Teléfono" class="form-control" value="<?= set_value('telefono') ?>" required>
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Dirección:</span>
                                    <input name="direccion" type="text" placeholder="Dirección"  class="form-control" value="<?= set_value('direccion') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Ciudad:</span>
                                    <input name="ciudad" type="text" placeholder="Ciudad" class="form-control" value="<?= set_value('ciudad') ?>" required>
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Departamento:</span>
                                    <input name="departamento" type="text" placeholder="Departamento"  class="form-control" value="<?= set_value('departamento') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Cód. Postal:</span>
                                    <input name="cod_postal" type="text" placeholder="Cod. Postal" class="form-control" value="<?= set_value('cod_postal') ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="planes_hospedaje">Planes de hospedaje:</label>
                                    <select name="planes_hospedaje" class="form-control" required="required">
                                        <option value="">Seleccione un plan</option>
                                        <?php if ($planes): ?>
                                            <?php foreach ($planes as $plan): ?>
                                                <option value="<?= $plan->id_planes ?>" <?= set_value('planes_hospedaje') == $plan->id_planes ? 'selected' : '' ?>><?= $plan->nombre_plan ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button class="btn btn-default pull-left " type="submit">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>


            <hr class="vertical-space2">
        </div>

        <!-- Contact form end
          ==================== -->

    </div>
</div>

<hr class="vertical-space2">
</section>


<!-- contact End
  ================================================== -->

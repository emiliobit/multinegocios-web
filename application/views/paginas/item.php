        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1><?= $items->nombre_item ?></h1>
                    </div>
                    <div class="col-md-5">
                    </div>
                </div>
            </div>
        </div>


<div id="content">
	<div class="container">
		<section>
			<div class="row">
				<div class="col-md-12">
					<div class="heading">
						<h2></h2>
					</div>
					<?php if(strlen($items->detalle) > 0 ): ?>
						 <p class="lead"><?= $items->detalle; ?></p>
					<?php endif; ?>
				</div>
			</div>
		</section>
	</div><!-- container -->
</div><!-- id="content" -->

<?php if ( ($items->formulario != 0) || is_null($items->formulario) ) {?>
<section class="mainContent">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center">
				<?php 
				if ($items->formulario == 1){ 
					echo  $this->load->view('paginas/form/check_dominio', NULL, TRUE) ;
				}elseif ($items->formulario == 2){ 
					echo  $this->load->view('paginas/form/check_py_dominio', NULL, TRUE) ;
				}elseif ($items->formulario == 3){
					echo  $this->load->view('paginas/form/form_whois_privado', NULL, TRUE) ;
				}elseif ($items->formulario == 4){
					echo  $this->load->view('paginas/form/form_consulta_whois', NULL, TRUE) ;
				}
				?>
			</div>
		</div>
	</div>
</section>
<?php } ?>



<!-- SERVICIOS -->
<?php if ($items->id_categoria_plan <> 0): ?>
<div class="content">
<div class="container">
			
<section>

                    <div class="row">
                        <div class="col-md-12">
							<?php foreach ($datos_planes as $datos_plan):  ?> 
								<div class="heading">
									<h3><?= $datos_plan->detalle;?></h3>
								</div>
							<?php endforeach ?>
                        </div>
                    </div>
                    <!-- /.row -->
					


                    <div class="row packages">
					
                      <?php  foreach ($planes as $item):  ?>
                        <div class="col-md-4">
                            <div class="best-value">
                                <div class="package">
                                    <div class="package-header">
                                        <h5><?php echo $item->nombre_plan; ?></h5>                                        
                                    </div>                                    
                                   <?php echo $item->descripcion_corta; ?>
                                    <a  href="<?= base_url();?>plan/<?php echo $item->id_planes; ?>" class="btn btn-template-main"> Detalles </a>
                                </div>
                            </div>
                        </div>
                       <?php endforeach; ?>
                    </div>

                </section>

</div>
</div>
<?php endif; ?>

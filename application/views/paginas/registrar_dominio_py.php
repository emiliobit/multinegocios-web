<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- H1 Header starts
  ================================================== -->
<section class="h1Header">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sectionTitle wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="500ms">
        <div class="container">
            <h4>Registrar dominio</h4>
            <h3>www.<?= html_escape($dominio) ?></h3>
            <!--
                <ul class="breadcrumbs">
                 <li><a href="index-2.html">Home</a> </li>
                <li><a href="contact.html">Contact</a></li>
                </ul>
            -->
        </div>
    </div>
</section>

<!-- H1 Header ends
  ================================================== -->

<!-- contact Start
  ================================================== -->
<section class="mainContent contactDetails">
    <div class="container">
        <hr class="vertical-space1">
        <div class="row">
            <!-- Contact form  start
              ==================== -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contactForm wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">

                <h4>Justificación</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                        <?= validation_errors() ?>
                        <?php if (isset($error_envio) && $error_envio === TRUE): ?>
                        <p>Ocurrio un error al intentar enviar los datos, por favor intente de nuevo</p>
                        <?php endif; ?>
                        <form action="<?= base_url('dominios/registrar_py') ?>" method="post" role="form">
						
                            <input type="hidden" name="dominio" value="<?= $dominio ?>">
							<input type="hidden" name="sitio_email" value="<?= $config->email_sitio; ?>">
                            <input type="hidden" name="reg_form" value="reg_form">
							
                            <div class="form-inline row">
								<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <span class="formInputName">Justificación</span>                                    
									<textarea name="justificacion"  placeholder="Justificación de Registro:" class="form-control" rows="2"  value="<?= set_value('nombre') ?>" required></textarea>
									<p>Describir el uso que se dará al dominio con relación a la empresa registrante, tipo de producto o servicio prestado por la empresa que será promocionado por medio del dominio. </p>
								</div>
								
							<h4>Complete el formulario con sus datos</h4>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Nombre:</span>
                                    <input name="nombre" type="text" placeholder="Nombre"  class="form-control" value="<?= set_value('nombre') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Apellido:</span>
                                    <input name="apellido" type="text" placeholder="Apellido"  class="form-control" value="<?= set_value('apellido') ?>" required>
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Empresa:</span>
                                    <input name="empresa" type="text" placeholder="Empresa (opcional)"  class="form-control" value="<?= set_value('empresa') ?>">
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Fax:</span>
                                    <input name="fax" type="tel" placeholder="Fax (opcional)" class="form-control" value="<?= set_value('fax') ?>">
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">CI:</span>
                                    <input name="cedula" type="text" placeholder="Cedula de identidad" class="form-control" value="<?= set_value('cedula') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">RUC:</span>
                                    <input name="ruc" type="text" placeholder="RUC (opcional)" class="form-control" value="<?= set_value('ruc') ?>">
                                </div>
                            </div>
							
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Email:</span>
                                    <input name="email" type="email" placeholder="Correo Electrónico"  class="form-control" value="<?= set_value('email') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Telefono:</span>
                                    <input name="telefono" type="tel" placeholder="Teléfono" class="form-control" value="<?= set_value('telefono') ?>" required>
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Dirección:</span>
                                    <input name="direccion" type="text" placeholder="Dirección"  class="form-control" value="<?= set_value('direccion') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Ciudad:</span>
                                    <input name="ciudad" type="text" placeholder="Ciudad" class="form-control" value="<?= set_value('ciudad') ?>" required>
                                </div>
                            </div>
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Departamento:</span>
                                    <input name="departamento" type="text" placeholder="Departamento"  class="form-control" value="<?= set_value('departamento') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Cód. Postal:</span>
                                    <input name="cod_postal" type="text" placeholder="Cod. Postal" class="form-control" value="<?= set_value('cod_postal') ?>" required>
                                </div>
                            </div>
							
							<div class="form-inline row">
								<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">                                    
                                    <select class="form-control" size="1" name="rubro">
										<option selected="selected" value="0">Rubro - Click para seleccionar</option>
										<option value="Sector Financiero, bancos, Inmuebles">Sector Financiero, bancos, Inmuebles</option>
										<option value="Hoteles y empresas de turismo">Hoteles y empresas de turismo</option>
										<option value="Prensa medios de comunicacion">Prensa medios de comunicacion</option>
										<option value="Telecomunicaciones e Internet">Telecomunicaciones e Internet</option>
										<option value="Informatica software, hardware">Informatica software, hardware</option>
										<option value="Construcciones e Ingeniería">Construcciones e Ingeniería</option>
										<option value="Sector de industra produccion y exportacion">Sector de industra produccion y exportacion</option>
										<option value="Importaciones, Distribucion y comercio de productos">Importaciones, Distribucion y comercio de productos</option>
										<option value="Ventas al consumidor y ventas al por mayor">Ventas al consumidor y ventas al por mayor</option>
										<option value="Proveedores de bienes y servicios">Proveedores de bienes y servicios</option>
										<option value="Servicios Medicos y de salud">Servicios Medicos y de salud</option>
										<option value="Organizaciones educativas y de Investigacion">Organizaciones educativas y de Investigacion</option>
										<option value="Sector Publico">Sector Publico</option>
										<option value="ONGs y organizaciones sin fines de lucro">ONGs y organizaciones sin fines de lucro</option>
										<option value="Entretenimietndo deportes, diversion, shows">Entretenimietndo deportes, diversion, shows</option>
										<option value="Abogados, servicios legales, escribanos">Abogados, servicios legales, escribanos</option>
										<option value="Profesionales Independientes">Profesionales Independientes</option>
										<option value="Representaciones Diplomáticas">Representaciones Diplomáticas</option>
										<option value="Organizaciones Cooperativas">Organizaciones Cooperativas</option>
									</select>
                                </div>
                            </div>
							
							</div>
							
                            <hr>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="planes_hospedaje">Planes de hospedaje:</label>
                                    <select name="planes_hospedaje" class="form-control" required="required">
                                        <option value="">Seleccione un plan</option>
                                        <?php if ($planes): ?>
                                            <?php foreach ($planes as $plan): ?>
                                                <option value="<?= $plan->id_planes ?>" <?= set_value('planes_hospedaje') == $plan->id_planes ? 'selected' : '' ?>><?= $plan->nombre_plan ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button class="btn btn-default pull-left " type="submit">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>


            <hr class="vertical-space2">
        </div>

        <!-- Contact form end
          ==================== -->

    </div>
</div>

<hr class="vertical-space2">
</section>


<!-- contact End
  ================================================== -->

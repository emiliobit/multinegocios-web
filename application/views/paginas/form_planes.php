<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- H1 Header starts
  ================================================== -->

<section class="h1Header">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sectionTitle wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="500ms">
        <div class="container">
            <h3>Solicitar plan</h3>
        </div>
    </div>
</section>

<section class="mainContent">
	<div class="container">  
		<div class="padre col-xs-12 col-lg-4 col-md-4 col-sm-4 c-plan-d" style="visibility: visible; animation-duration: 500ms; animation-delay: 0.2s; animation-name: fadeInUp;">
			<h2 class="hijo"><span id="circulo"><p class="small">Nombre </p><?= $plan_dato->nombre_plan ?></span></h2>
		</div>
		<div class="padre col-xs-12 col-lg-4 col-md-4 col-sm-4 c-plan-d"  style="visibility: visible; animation-duration: 500ms; animation-delay: 0.3s; animation-name: fadeInUp;">
			<h2 class="hijo"><span id="circulo"><p class="small">Espacio </p><?= $plan_dato->espacio ?></span></h2>
		</div>
		<div class="padre col-xs-12 col-lg-4 col-md-4 col-sm-4 c-plan-d"  style="visibility: visible; animation-duration: 500ms; animation-delay: 0.4s; animation-name: fadeInUp;">
			<h2 class="hijo"><span id="circulo"><p class="small">mensual</p>Gs.<?= $plan_dato->costo ?><p class="small">Mensual</p></span></h2>
		</div>
		</div>
	</div>
</section>

<!-- H1 Header ends
  ================================================== -->

<!-- contact Start
  ================================================== -->
<section class="mainContent contactDetails">
    <div class="container">
        <hr class="vertical-space1">
        <div class="row">
            <!-- Contact form  start
              ==================== -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contactForm wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">

                
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                      
                        <?php if (validation_errors()): ?>
						
						<div class="alert alert-error">
						<p>Ocurrio un error al intentar enviar los datos, por favor intente de nuevo</p>
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong> <?= validation_errors() ?></strong> 
						</div> 						
                        <?php endif; ?>
						
						
                        <form action="<?= base_url('planes/solicitar').'/'.$plan_dato->id_planes ?>" method="post" role="form">
						
                            <input type="hidden" name="plan" value="<?= $plan_dato->nombre_plan ?>">
                            <input type="hidden" name="solicitud_form" value="solicitud_form">
							<input type="hidden" name="sitio_email" value="<?= $config->email_sitio; ?>">							
							<input type="hidden" name="ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
							
                            <div class="form-inline row">																
							<h4>Complete el formulario con sus datos</h4>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Nombre:</span>
                                    <input name="nombre" type="text" placeholder="Nombre"  class="form-control" value="<?= set_value('nombre') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Apellido:</span>
                                    <input name="apellido" type="text" placeholder="Apellido"  class="form-control" value="<?= set_value('apellido') ?>" required>
                                </div>
                            </div>
							
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Empresa:</span>
                                    <input name="empresa" type="text" placeholder="Empresa (opcional)"  class="form-control" value="<?= set_value('empresa') ?>">
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Fax:</span>
                                    <input name="fax" type="tel" placeholder="Fax (opcional)" class="form-control" value="<?= set_value('fax') ?>">
                                </div>
                            </div>
							
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">CI:</span>
                                    <input name="cedula" type="text" placeholder="Cedula de identidad" class="form-control" value="<?= set_value('cedula') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">RUC:</span>
                                    <input name="ruc" type="text" placeholder="RUC (opcional)" class="form-control" value="<?= set_value('ruc') ?>">
                                </div>
                            </div>
							
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Email:</span>
                                    <input name="email" type="email" placeholder="Correo Electrónico"  class="form-control" value="<?= set_value('email') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Telefono:</span>
                                    <input name="telefono" type="tel" placeholder="Teléfono" class="form-control" value="<?= set_value('telefono') ?>" required>
                                </div>
                            </div>
							
                            <div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Dirección:</span>
                                    <input name="direccion" type="text" placeholder="Dirección"  class="form-control" value="<?= set_value('direccion') ?>" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span class="formInputName">Ciudad:</span>
                                    <input name="ciudad" type="text" placeholder="Ciudad" class="form-control" value="<?= set_value('ciudad') ?>" required>
                                </div>
                            </div>
                            
							<hr>
							
						<div class="form-inline row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<p class="help-inline">SÍ acepto los terminos y condiciones de uso.</p>
									<label class="checkbox"><input name="terminos" type="checkbox" value="Accepto" >Acepto</label> 								 
									<p class="help-inline"><a href="<?= base_url(); ?>condiciones_uso">Leer terminos y condiciones de uso</a></p>
								</div>		
								
								<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <p class="help-inline">Por motivos de seguridad estaremos registrando:</p>							
									<span><?php 
											echo "- Su dirección IP: {$_SERVER['REMOTE_ADDR']}<br>"; 
											//echo "- El nombre de su servidor: {$_SERVER['SERVER_NAME']}<br>"; 
											//echo "- Procedente de la página: {$_SERVER['HTTP_REFERER']}<br>"; 
											//echo "- Su puerto es el: {$_SERVER['REMOTE_PORT']}<br>"; 
											//echo "- Su navegador es: {$_SERVER['HTTP_USER_AGENT']}";
											?>
								</span>
								</div>
                                
                            </div>
							
                            <hr>
                           
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button class="btn btn-default pull-left btn-lg " type="submit">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>


            <hr class="vertical-space2">
        </div>

        <!-- Contact form end
          ==================== -->

    </div>
</div>

<hr class="vertical-space2">
</section>


<!-- contact End
  ================================================== -->

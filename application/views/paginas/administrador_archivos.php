<?php 
//session_start();
//require_once("../config.inc.php");
// Si no existe la variable usuario redireccionar a login.php
/*
if (!isset ($_SESSION['usuario']))
{
	header ('location: login.php');
}
$usuario = $_SESSION['usuario'];
*/
$base_url = "http://localhost/ndedominios-CI/";
//$base_url = "http://www.ndedominios.com/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo ($GLOBALS['sitio_nombre']) ?> - Administrador de Archivos</title>

<link rel="stylesheet" href="<?php echo $base_url ?>/app/jquery/css/jquery-ui-1.8.6.custom.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $base_url ?>/app/elfinder/css/elfinder.css" type="text/css" />
<link rel="stylesheet" href="css/estilo.css" type="text/css" />

<script src="<?php echo $base_url ?>/app/jquery/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url ?>/app/jquery/js/jquery-ui-1.8.6.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url ?>/app/elfinder/js/elfinder.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url ?>/app/elfinder/js/i18n/elfinder.es.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript">
$().ready(function() {	
	var f = $('#finder').elfinder({
		url : '../app/elfinder/connectors/php/connector.php',
		lang : 'es'
	})
})
</script>

</head>

<body>
<h3>Administrador de Imagenes</h3>

<hr align="center" size="1" />
<?php 
//require_once("menu.php");
?>
<br />
<div id="finder">Administrador de archivo</div>
</body>
</html>

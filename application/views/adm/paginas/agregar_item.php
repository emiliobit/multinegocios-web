<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Agregar item</h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos del item</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/items/agregar')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre_plan">Nombre item</label>
                            <input type="text" placeholder="Nombre item" name="nombre_item" class="form-control" value="<?= set_value('nombre_item') ?>" autofocus required>
                            <?= form_error('nombre_item', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                       	<div class="form-group">
                            <label>Categoria</label>
                            <select id="categoria_item" name="id_categoria_item" class="form-control" style="width: 100%;" required>
                                <option value="">Selecione categoria</option>
                                <?php if ($categorias_item): ?>
                                    <?php foreach ($categorias_item as $cat_item): ?>
                                        <option value="<?= $cat_item->id_categoria_item ?>" <?= set_value('id_categoria_item') == $cat_item->id_categoria_item ? 'selected' : null ?>><?= html_escape($cat_item->nombre_cat) ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <?= form_error('id_categoria_item', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

                        <div class="form-group">
                            <label>Plan relacionado</label>
                            <select id="categoria_plan" name="id_categoria_plan" class="form-control" style="width: 100%;">
                                <option value="">Selecione Plan relacionado</option>
                                <?php if ($categorias_plan): ?>
                                    <?php foreach ($categorias_plan as $cat_plan): ?>
                                        <option value="<?= $cat_plan->id_categoria_plan ?>" <?= set_value('id_categoria_plan') == $cat_plan->id_categoria_plan ? 'selected' : null ?>><?= html_escape($cat_plan->nombre_plan) ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <?= form_error('id_categoria_plan', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="box-body pad">
                            <label for="descripcion_corta">Contenido</label>
                            <textarea name="detalle" id="textarea" placeholder="Escribe aquí..." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required><?= set_value('detalle', '', false) ?></textarea>
                            <?= form_error('detalle', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Habilitado</label>
                            <div class="radio">
                                <label><input type="radio" <?= set_value('habilitado', NULL) === NULL ? 'checked' : '' ?> value="1" id="optionsRadios1" name="habilitado" <?= set_value('habilitado') === '1' ? 'checked' : '' ?> required>Si</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value="0" id="optionsRadios2" name="habilitado" <?= set_value('habilitado') === '0' ? 'checked' : '' ?> required>No</label>
                            </div>
                            <?= form_error('habilitado', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Formulario</label>
                            <select id="form_item" name="formulario" class="form-control" style="width: 100%;">
                                <option value="">Seleccione un formulario</option>
                                <option value="1" <?= set_value('formulario') == '1' ? 'selected' : null ?>>Formulario Dominios</option>
                                <option value="2" <?= set_value('formulario') == '2' ? 'selected' : null ?>>Formulario Dominios .py</option>
                                <option value="3" <?= set_value('formulario') == '3' ? 'selected' : null ?>>Formulario Whois Privado</option>
								<option value="4" <?= set_value('formulario') == '4' ? 'selected' : null ?>>Formulario Consulta Whois</option>
							</select>
                            <?= form_error('formulario', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm/items') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
<script>
    CKEDITOR.replace('textarea');
</script>
<script>
    $(function () {
        $('#categoria_plan').select2();
    });
</script>
<script>
    $(function () {
        $('#categoria_item').select2();
    });
</script>
<script>
    $(function () {
        $('#form_item').select2();
    });
</script>
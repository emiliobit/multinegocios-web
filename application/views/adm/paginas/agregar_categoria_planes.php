<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>
        Tipo de Plan
        <small>Agregar</small>
    </h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tipo de plan</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/categoria_planes/agregar')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre_plan">Nombre plan</label>
                            <input type="text" placeholder="Nombre tipo de plan" name="nombre_plan" class="form-control" value="<?= set_value('nombre_plan') ?>" autofocus required>
                            <?= form_error('nombre_plan', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="box-body pad">
                            <label>Detalle</label>
                            <textarea name="detalle" id="textarea" placeholder="Escribe aquí..." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required><?= set_value('detalle', '', false) ?></textarea>
                            <?= form_error('detalle', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Habilitado</label>
                        <div class="radio">
                            <label><input type="radio" <?= set_value('habilitado', NULL) === NULL ? 'checked' : '' ?> value="1" id="optionsRadios1" name="habilitado" <?= set_value('habilitado') === '1' ? 'checked' : '' ?> required>Si</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" value="0" id="optionsRadios2" name="habilitado" <?= set_value('habilitado') === '0' ? 'checked' : '' ?> required>No</label>
                        </div>
                        <?= form_error('habilitado', '<p style="color: #d73925;">', '</p>') ?>
                    </div>
                    <div class="form-group">
                        <label>Destacado</label>
                        <div class="radio">
                            <label><input type="radio" <?= set_value('destacado', NULL) === NULL ? 'checked' : '' ?> value="1" id="optionsRadios1" name="destacado" <?= set_value('destacado') === '1' ? 'checked' : '' ?> required>Si</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" value="0" id="optionsRadios2" name="destacado" <?= set_value('habilitado') === '0' ? 'checked' : '' ?> required>No</label>
                        </div>
                        <?= form_error('destacado', '<p style="color: #d73925;">', '</p>') ?>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm/categoria_planes') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
<script>
    CKEDITOR.replace('textarea');
</script>

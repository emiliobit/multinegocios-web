<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--Content Wrapper. Contains page content -->
<!--Content Header (Page header) -->

<section class = "content-header">
    <h1>
        Items
        <small></small>
    </h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Lista de Items</h3>
                </div>
                <div class="box-header">
                    <a class="btn btn-primary" href="<?= base_url('adm/items/agregar') ?>">Agregar</a>
                </div>
                <div class="box-body">
                    <table id="lista" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Categoria</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Categoria</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php if ($items): ?>
                                <?php foreach ($items as $item): ?>
                                    <tr>
                                        <td><?= html_escape($item->nombre_item) ?></td>
                                        <td><?= html_escape($item->categoria_item->nombre_cat) ?></td>
                                        <td>
											<a href="<?= base_url('adm/items/habilitado/'.$item->id_item.'/'.$item->habilitado) ?>"
												<?php echo ($item->habilitado == 1) ? '<span title="" data-toggle="tooltip" data-original-title="Habilitado"><span style="color:#3c8dbc; font-size:20px" class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></span>' : '<span title="" data-toggle="tooltip" data-original-title="No esta habilitado"><span style="color:#d73925; font-size:20px" class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span></span>'; ?>
											</a>
										</td>
                                        <td>
                                            <a href="<?= base_url('adm/items/editar/'.$item->id_item) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                            <?= form_open(base_url('adm/items/eliminar/'.$item->id_item), array('style' => 'display: inline !important')); ?>
                                            <button name="delete-btn" class="btn btn-danger" type="button">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                            <?= form_close() ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section><!--/.content -->
<div id="delete-confirm-modal" class="modal modal-primary">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Eliminar</h4>
            </div>
            <div class="modal-body">
                <p><?= html_escape('¿Desea eliminar el registro?') ?></p>
            </div>
            <div class="modal-footer">
                <button id="delete-btn-cancel"  type="button" class="btn btn-outline pull-left">No</button>
                <button id="delete-btn-confirm" type="button" class="btn btn-outline">Sí</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    // Delete button EVENTS
    var form;
    $('button[name="delete-btn"]').on('click', function (e) {
        e.preventDefault();
        form = $(this).closest('form');
        var deleteConfirmModal = $('#delete-confirm-modal');
        deleteConfirmModal.modal({backdrop: 'static', keyboard: false});
        deleteConfirmModal.one('click', '#delete-btn-cancel', function () {
            deleteConfirmModal.modal('toggle');
        });
    });

    $('#delete-btn-confirm').on('click', function (e) {
        e.preventDefault();
        form.trigger('submit'); // submit the form
    });
</script>
<script>
    $(function () {
        $('#lista').DataTable({
            "responsive": true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "language": {
                "url": "<?= base_url('assets/adminlte/plugins/datatables/Spanish.json') ?>"
            },
            "columnDefs": [{
                    "targets": -1,
                    "searchable": false,
                    "orderable": false,
                    "visible": true,
                    "responsivePriority": 1
                }]
        });
    });
</script>

<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Agregar banner</h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos del banner</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open_multipart(base_url('adm/banners/agregar')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Banner</label>
                            <input type="file" name="imagen" required>
                            <?= form_error('imagen', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Orden 1 - 10</label>
                            <input type="number" placeholder="Ordel del banner" name="orden" class="form-control" value="<?= set_value('orden') ?>" min="1" max="10" required>
                            <?= form_error('orden', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Texto 1</label>
                            <input type="text" placeholder="Texto aqui..." name="texto1" class="form-control" value="<?= set_value('texto1') ?>">
                            <?= form_error('texto1', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Texto 2</label>
                            <input type="text" placeholder="Texto aqui..." name="texto2" class="form-control" value="<?= set_value('texto2') ?>">
                            <?= form_error('texto2', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Texto 3</label>
                            <input type="text" placeholder="Texto aqui..." name="texto3" class="form-control" value="<?= set_value('texto3') ?>">
                            <?= form_error('texto3', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						<div class="form-group">
							<label>Habilitado</label>
							<div class="radio">
								<label><input type="radio" <?= set_value('habilitado', NULL) === NULL ? 'checked' : '' ?> value="1" id="optionsRadios1" name="habilitado" <?= set_value('habilitado') === '1' ? 'checked' : '' ?> required>Si</label>
							</div>
							<div class="radio">
								<label><input type="radio" value="0" id="optionsRadios2" name="habilitado" <?= set_value('habilitado') === '0' ? 'checked' : '' ?> required>No</label>
							</div>
						<?= form_error('habilitado', '<p style="color: #d73925;">', '</p>') ?>
						</div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm/banners') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
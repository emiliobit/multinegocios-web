<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>
        Plan
        <small>Agregar</small>
    </h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Editar plan</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/planes/editar/' . $plan->id_planes)) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre_plan">Nombre plan</label>
                            <input type="text" placeholder="Nombre plan" name="nombre_plan" class="form-control" value="<?= set_value('nombre_plan', $plan->nombre_plan) ?>" autofocus required>
                            <?= form_error('nombre_plan', '<p style="color: #d73925;">', '</p>') ?>
                            <?php if (isset($form_error['nombre_plan'])): ?>
                                <p style="color: #d73925;"><?= $form_error['nombre_plan'] ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label for="costo">Costo</label>
                            <input type="text" placeholder="Costo" name="costo" class="form-control" value="<?= set_value('costo', $plan->costo) ?>" required>
                            <?= form_error('costo', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label for="espacio">Espacio</label>
                            <input type="text" placeholder="Espacio" name="espacio" class="form-control" value="<?= set_value('espacio', $plan->espacio) ?>" required>
                            <?= form_error('espacio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Categoria</label>
                            <select id="categoria_plan" name="id_categoria_plan" class="form-control" style="width: 100%;" required>
                                <option value="">Seleccione una categoria</option>
                                <?php if ($categorias): ?>
                                    <?php foreach ($categorias as $categoria): ?>
                                        <option value="<?= $categoria->id_categoria_plan ?>" <?= set_value('id_categoria_plan', $plan->id_categoria_plan) == $categoria->id_categoria_plan ? 'selected' : null ?>><?= html_escape($categoria->nombre_plan) ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <?= form_error('id_categoria_plan', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="box-body pad">
                            <label>Descripcion corta</label>
                            <textarea name="descripcion_corta" id="desc_corta" placeholder="Escribe aquí..." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required><?= set_value('descripcion_corta', $plan->descripcion_corta, false) ?></textarea>
                            <?= form_error('descripcion_corta', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="box-body pad">
                            <label>Descripcion detallada</label>
                            <textarea name="descripcion_detalle" id="textarea" placeholder="Escribe aquí..." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required><?= set_value('descripcion_detalle', $plan->descripcion_detalle, false) ?></textarea>
                            <?= form_error('descripcion_detalle', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
							<label>Habilitado</label>
							<div class="radio">
								<label><input type="radio" value="1" id="optionsRadios1" name="habilitado" <?= set_value('habilitado', $plan->habilitado) === '1' ? 'checked' : '' ?> required>Si</label>
							</div>
							<div class="radio">
								<label><input type="radio" value="0" id="optionsRadios2" name="habilitado" <?= set_value('habilitado', $plan->habilitado) === '0' ? 'checked' : '' ?> required>No</label>
							</div>
						<?= form_error('habilitado', '<p style="color: #d73925;">', '</p>') ?>
						</div>
                        <div class="form-group">
							<label>Destacado</label>
							<div class="radio">
								<label><input type="radio" value="1" id="optionsRadios1" name="destacado" <?= set_value('destacado', $plan->destacado) === '1' ? 'checked' : '' ?> required>Si</label>
							</div>
							<div class="radio">
								<label><input type="radio" value="0" id="optionsRadios2" name="destacado" <?= set_value('destacado', $plan->destacado) === '0' ? 'checked' : '' ?> required>No</label>
							</div>
						<?= form_error('destacado', '<p style="color: #d73925;">', '</p>') ?>
						</div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm/planes') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
<script>
    CKEDITOR.replace('textarea');
</script>
<script>
    CKEDITOR.replace('desc_corta');
</script>
<script>
    $(function () {
        $('#categoria_plan').select2();
    });
</script>

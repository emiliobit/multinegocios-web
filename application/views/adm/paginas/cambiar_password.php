<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Cambiar Contraseña</h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Nueva Contraseña</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/perfil/cambiar-pass/')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <p>La contraseña debe tener como mínimo 5 caracteres</p>
                            <label>Contraseña Actual</label>
                            <input type="password" placeholder="Contraseña Actual" name="password_actual" class="form-control" value="" minlength="5" autofocus required>
                            <?= form_error('password_actual', '<p style="color: #d73925;">', '</p>') ?>
                            <?php if (isset($error)): ?>
                                <p style="color: #d73925;"><?= $error ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label>Contraseña Nueva</label>
                            <input type="password" placeholder="Contraseña Nueva" name="password_nuevo" class="form-control" value="" minlength="5" required>
                            <?= form_error('password_nuevo', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Repetir Contraseña</label>
                            <input type="password" placeholder="Repetir Contraseña" name="password_nuevo_rep" class="form-control" value="" minlength="5" required>
                            <?= form_error('password_nuevo_rep', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Editar Perfil</h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/perfil/editar/')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Contraseña Actual</label>
                            <input type="password" placeholder="Contraseña Actual" name="password" class="form-control" value="" minlength="5" autofocus required>
                            <?= form_error('password', '<p style="color: #d73925;">', '</p>') ?>
                            <?php if (isset($error)): ?>
                                <p style="color: #d73925;"><?= $error ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" placeholder="Nombre" name="name" class="form-control" value="<?= set_value('name', $user->name) ?>" minlength="5" required>
                            <?= form_error('name', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" placeholder="Apellido" name="last_name" class="form-control" value="<?= set_value('last_name', $user->last_name) ?>" minlength="5" required>
                            <?= form_error('last_name', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" placeholder="Email" name="email" class="form-control" value="<?= set_value('email', $user->email) ?>" minlength="5" required>
                            <?= form_error('email', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->

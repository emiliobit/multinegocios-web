<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Configuracion del Sitio - Avanzado</h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/configuraciones-avanzadas/')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Estado del Sitio</label>
                            <div class="radio">
                                <label><input type="radio" value="1" id="optionsRadios1" name="estado_sitio" <?= set_value('estado_sitio', $config->estado_sitio) === '1' ? 'checked' : '' ?> required>Si</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value="0" id="optionsRadios2" name="estado_sitio" <?= set_value('estado_sitio', $config->estado_sitio) === '0' ? 'checked' : '' ?> required>No</label>
                            </div>
                            <?= form_error('estado_sitio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
<script>
    CKEDITOR.replace('detalle_dominio');
</script>
<script>
    CKEDITOR.replace('detalle_dominio_py');
</script>
<script>
    $(function () {
        $('#plan_hospedaje').select2();
    });
</script>
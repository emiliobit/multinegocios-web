<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Subir Imagen</h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open_multipart(base_url('adm/imagenes/agregar')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Seleccione Imagen</label>
                            <input type="file" name="imagen" required>
                            <?= form_error('imagen', '<p style="color: #d73925;">', '</p>') ?>
                        </div> 
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm/portada') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--Content Wrapper. Contains page content -->
<!--Content Header (Page header) -->

<section class = "content-header">
    <h1>Inicio<small></small>
    </h1>
</section>

<!--Main content -->
<section class = "content">
<div class="row">
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $items_cant; ?></h3>
              <p>Items</p>
            </div>
            <div class="icon">
              <i class="ion ion-document-text"></i>
            </div>
              <a class="small-box-footer" href="<?= base_url() ?>adm/items">Mas <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $cat_item_cant ?></h3>
              <p>Categorias</p>
            </div>
            <div class="icon">
              <i class="ion-ios-list-outline"></i>
            </div>
            <a class="small-box-footer" href="<?= base_url() ?>adm/categoria_items">Mas <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->


        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $planes_cant ?></h3>
              <p>Planes</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-pricetags"></i>
            </div>
            <a class="small-box-footer" href="<?= base_url() ?>adm/planes">Mas <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $cat_plan_cant ?></h3>
              <p>Tipos de planes</p>
            </div>
            <div class="icon">
              <i class="ion-clipboard"></i>
            </div>
            <a class="small-box-footer" href="<?= base_url() ?>adm/categoria_planes">Mas <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
</section><!--/.content -->
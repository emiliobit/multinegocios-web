<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Agregar Usuario</h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos del usuario</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/usuarios/agregar')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" placeholder="correo@ejemplo.com" class="form-control" name="email" value="<?= set_value('email') ?>" required>
                            <?= form_error('email', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" placeholder="Nombre" name="name" class="form-control" value="<?= set_value('name') ?>" required>
                            <?= form_error('name', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" placeholder="Apellido" name="last_name" class="form-control" value="<?= set_value('last_name') ?>" required>
                            <?= form_error('last_name', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" placeholder="Contraseña" name="password" class="form-control" required>
                            <?= form_error('password', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						<div class="form-group">
							<label>Habilitado</label>
							<div class="radio">
								<label><input type="radio" <?= set_value('active', null) === null ? 'checked' : '' ?> value="1" id="optionsRadios1" name="active" <?= set_value('active') === '1' ? 'checked' : '' ?> required>Si</label>
							</div>
							<div class="radio">
								<label><input type="radio" value="0" id="optionsRadios2" name="active" <?= set_value('active') === '0' ? 'checked' : '' ?> required>No</label>
							</div>
						        <?= form_error('active', '<p style="color: #d73925;">', '</p>') ?>
						</div>
                        <div class="form-group">
                            <label>Tipo</label>
                            <select id="group_name" name="group_name" class="form-control" style="width: 100%;" required>
                                <option value="">Elija el tipo de usuario</option>
                                <option value="admin" <?= set_value('group_name', null) === "admin" ? 'selected' : null ?>>Administrador</option>
                                <option value="staff" <?= set_value('group_name', null) === "staff" ? 'selected' : null ?>>Miembro Staff</option>
                            </select>
                            <?= form_error('group_name', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm/usuarios') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->

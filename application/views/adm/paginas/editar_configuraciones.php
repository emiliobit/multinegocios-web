<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>Configuracion del Sitio</h1>
</section>
<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Datos</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/configuraciones/')) ?>
                <div class="box-body">
                    <div class="col-md-12">
						<div class="form-group">
                            <label for="nombre_plan">Nombre del sitio</label>
                            <input type="text" placeholder="Nombre del sitio"  class="form-control" name ="nombre_sitio" value="<?= set_value('nombre_sitio', $config->nombre_sitio) ?>" autofocus required>
                            <?= form_error('nombre_sitio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Descripcion</label>
                            <input type="text" placeholder="Descripcion del sitio"  class="form-control" name ="descripcion_sitio" value="<?= set_value('descripcion_sitio', $config->descripcion_sitio) ?>" required>
                            <?= form_error('descripcion_sitio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>



						<div class="form-group">
                            <label for="nombre_plan">Email</label>
                            <input type="text" placeholder="Email del sitio"  class="form-control" name ="email_sitio" value="<?= set_value('email_sitio', $config->email_sitio) ?>" required>
                            <?= form_error('email_sitio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Telefono 1</label>
                            <input type="text" placeholder="Telefono del sitio..."  class="form-control" name ="telefono1_sitio" value="<?= set_value('telefono1_sitio', $config->telefono1_sitio) ?>" >
                            <?= form_error('telefono1_sitio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Telefono 2</label>
                            <input type="text" placeholder="Telefono del sitio..."  class="form-control" name ="telefono2_sitio" value="<?= set_value('telefono2_sitio', $config->telefono2_sitio) ?>" >
                            <?= form_error('telefono2_sitio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<div class="form-group">
                            <label for="nombre_plan">Simbolo Moneda</label>
                            <input type="text" placeholder="Simbolo Moneda..."  class="form-control" name ="simbolo_moneda" value="<?= set_value('simbolo_moneda', $config->simbolo_moneda) ?>" >
                            <?= form_error('simbolo_moneda', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Costo Dominio</label>
                            <input type="text" placeholder="Costo dominio..."  class="form-control" name ="costo_dominio" value="<?= set_value('costo_dominio', $config->costo_dominio) ?>" required>
                            <?= form_error('costo_dominio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Costo Dominios Py</label>
                            <input type="text" placeholder="Costo de registro de dominios .py..."  class="form-control" name ="costo_dominio_py" value="<?= set_value('costo_dominio_py', $config->costo_dominio_py) ?>" required>
                            <?= form_error('costo_dominio_py', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Detalle Dominios</label>
							<textarea type="text" id="det_dominio" placeholder="Descripcion del registro de dominios..."  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name ="detalle_dominio" required><?= set_value('detalle_dominio', $config->detalle_dominio, FALSE) ?></textarea>
                           <?= form_error('detalle_dominio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Detalle Dominios .py</label>
                            <textarea type="text" id="det_dominio_py" placeholder="Descripcion del registro de dominios .py..."  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name ="detalle_dominio_py" required><?= set_value('detalle_dominio_py', $config->detalle_dominio_py, FALSE) ?></textarea>
                            <?= form_error('detalle_dominio_py', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<div class="form-group">
                            <label for="nombre_plan">Condiciones de Uso</label>
                            <textarea type="text" id="condicion_uso" placeholder="Condiciones de uso..."  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name ="condicion_uso" required><?= set_value('condicion_uso', $config->condicion_uso, FALSE) ?></textarea>
                            <?= form_error('condicion_uso', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<div class="form-group">
                            <label for="nombre_plan">Facebook</label>
                            <input type="text" placeholder="Enlace a Facebook..."  class="form-control" name ="red_social_1" value="<?= set_value('red_social_1', $config->red_social_1) ?>" required>
                            <?= form_error('red_social_1', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Twitter</label>
                            <input type="text" placeholder="Enlace a Twitter..."  class="form-control" name ="red_social_2" value="<?= set_value('red_social_2', $config->red_social_2) ?>" required>
                            <?= form_error('red_social_2', '<p style="color: #d73925;">', '</p>') ?>
                        </div>

						<div class="form-group">
                            <label for="nombre_plan">Instagram</label>
                            <input type="text" placeholder="Enlace a Instagram..."  class="form-control" name ="red_social_3" value="<?= set_value('red_social_3', $config->red_social_3) ?>" required>
                            <?= form_error('red_social_3', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<!--portada-->
						
						<div class="form-group">
                            <label for="nombre_plan">Portada Texto 1</label>
                            <textarea type="text" id="portada_texto_1" placeholder="Portada texto 1"  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name ="portada_texto_1" required><?= set_value('portada_texto_1', $config->portada_texto_1, FALSE) ?></textarea>
                            <?= form_error('portada_texto_1', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<div class="form-group">
                            <label for="nombre_plan">Portada Texto 2</label>
                            <textarea type="text" id="portada_texto_2" placeholder="Portada texto 2"  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name ="portada_texto_2" required><?= set_value('portada_texto_2', $config->portada_texto_2, FALSE) ?></textarea>
                            <?= form_error('portada_texto_2', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<div class="form-group">
                            <label for="nombre_plan">Texto Portada Dominios </label>
                            <textarea type="text" id="portada_texto_dom" placeholder="portada_texto_dom"  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name ="portada_texto_dom" required><?= set_value('portada_texto_dom', $config->portada_texto_dom, FALSE) ?></textarea>
                            <?= form_error('portada_texto_dom', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<div class="form-group">
                            <label for="nombre_plan">Texto Portada Dominios Py </label>
                            <textarea type="text" id="portada_texto_dom_py" placeholder="portada_texto_dom_py"  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name ="portada_texto_dom_py" required><?= set_value('portada_texto_dom_py', $config->portada_texto_dom_py, FALSE) ?></textarea>
                            <?= form_error('portada_texto_dom_py', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						
						<!--fin portada-->				
						
						
						
                        <div class="form-group">
                            <label>Plan de Hospedaje</label>
                            <select id="plan_hospedaje" name="plan_reg_dominio" class="form-control" style="width: 100%;" required>
                                <option value="">Seleccione el plan de hospedaje</option>
                                <?php if ($categorias): ?>
                                    <?php foreach ($categorias as $categoria): ?>
                                        <option value="<?= $categoria->id_categoria_plan ?>" <?= set_value('plan_reg_dominio', $config->plan_reg_dominio) == $categoria->id_categoria_plan ? 'selected' : null ?>><?= html_escape($categoria->nombre_plan) ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <?= form_error('plan_reg_dominio', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
<script>
    CKEDITOR.replace('detalle_dominio');
</script>
<script>
    CKEDITOR.replace('detalle_dominio_py');
</script>
<script>
    CKEDITOR.replace('condicion_uso');
</script>
<script>
    CKEDITOR.replace('portada_texto_1');
</script>
<script>
    CKEDITOR.replace('portada_texto_2');
</script>
<script>
    CKEDITOR.replace('portada_texto_dom');
</script>
<script>
    CKEDITOR.replace('portada_texto_dom_py');
</script>


<script>
    $(function () {
        $('#plan_hospedaje').select2();
    });
</script>
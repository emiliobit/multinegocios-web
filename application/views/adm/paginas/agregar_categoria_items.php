<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--Content Header (Page header) -->
<section class = "content-header">
    <h1>
        Categoria
        <small>Agregar</small>
    </h1>
</section>

<!--Main content -->
<section class = "content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar categoria</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?= form_open(base_url('adm/categoria_items/agregar')) ?>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre_cat">Nombre Categoria</label>
                            <input type="text" placeholder="Nombre de categoria" name="nombre_cat" class="form-control" value="<?= set_value('nombre_cat') ?>" autofocus required>
                            <?= form_error('nombre_cat', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripcion</label>
                            <input type="text" placeholder="Descripcion" name="descripcion" class="form-control" value="<?= set_value('descripcion') ?>" required>
                            <?= form_error('descripcion', '<p style="color: #d73925;">', '</p>') ?>
                        </div>
						<div class="form-group">
							<label for="nombre_cat">Habilitado</label>
							<div class="radio">
								<label><input type="radio" <?= set_value('habilitado', NULL) === NULL ? 'checked' : '' ?> value="1" id="optionsRadios1" name="habilitado" <?= set_value('habilitado') === '1' ? 'checked' : '' ?> required>Si</label>
							</div>
							<div class="radio">
								<label><input type="radio" value="0" id="optionsRadios2" name="habilitado" <?= set_value('habilitado') === '0' ? 'checked' : '' ?> required>No</label>
							</div>
						<?= form_error('habilitado', '<p style="color: #d73925;">', '</p>') ?>
						</div>

                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="<?= base_url('adm/categoria_items') ?>" class="btn btn-default">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div><!-- /.box -->
        </div>
    </div>
</section><!--/.content -->
<script>
    CKEDITOR.replace('textarea');
</script>
<script>
    $(function () {
        $('#categoria_plan').select2();
    });
</script>

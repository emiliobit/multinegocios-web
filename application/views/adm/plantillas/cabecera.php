<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $config->nombre_sitio?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Title icon -->
        <link rel="shortcut icon" href="<?= base_url('/asset_2/images/favicon.jpg') ?>" />
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?= base_url('assets/adminlte/bootstrap/css/bootstrap.min.css') ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url('assets/adminlte/plugins/font-awesome-4.5.0/css/font-awesome.min.css') ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url('assets/adminlte/plugins/ionicons-2.0.1/css/ionicons.min.css') ?>">

        <!-- Generated -->
        <?php if (isset($css_files)): ?>
            <?php foreach ($css_files as $css_file): ?>
                <link rel="stylesheet" href="<?= base_url($css_file) ?>">
            <?php endforeach; ?>
        <?php endif; ?>

        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url('assets/adminlte/dist/css/AdminLTE.min.css') ?>">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link rel="stylesheet" href="<?= base_url('assets/adminlte/dist/css/skins/skin-blue.min.css') ?>">



        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 2.1.4 -->
        <script src="<?= base_url('assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') ?>"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?= base_url('assets/adminlte/bootstrap/js/bootstrap.min.js') ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url('assets/adminlte/dist/js/app.min.js') ?>"></script>
        <script src="<?= base_url('assets/adminlte/plugins/fastclick/fastclick.min.js') ?>"></script>
        <script src="<?= base_url('assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>

        <?php if (isset($js_files)): ?>
            <?php foreach ($js_files as $js_file): ?>
                <script src="<?= base_url($js_file) ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="<?= base_url('assets/adminlte/plugins/iexplorer/html5shiv.min.js') ?>"></script>
            <script src="<?= base_url('assets/adminlte/plugins/iexplorer/respond.min.js') ?>"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini fixed">
        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">

                <!-- Logo -->
                <a href="<?= base_url() ?>" class="logo" target="_blank">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><?= $config->nombre_sitio?></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><?= $config->nombre_sitio ?></span>
                </a>

                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <?php $this->load->view('adm/plantillas/retazos/right_menu') ?>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <?php $this->load->view('adm/plantillas/retazos/left_sidebar'); ?>
            <!-- Control Sidebar -->

            <div class = "content-wrapper">
            <?= $this->flash_message->show_message();

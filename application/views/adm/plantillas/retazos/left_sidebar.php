<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Navegaci&oacute;n</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="<?= base_url('adm/portada') ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
            <li><a href="<?= base_url('adm/categoria_items') ?>"><i class="fa fa-files-o"></i> <span>Categorias</span></a></li>
            <li><a href="<?= base_url('adm/items') ?>"><i class="fa  fa-file-text"></i> <span>Items</span></a></li>
            <li><a href="<?= base_url('adm/categoria_planes') ?>"><i class="fa fa-tags"></i> <span>Tipo de Planes</span></a></li>
            <li><a href="<?= base_url('adm/planes') ?>"><i class="fa fa-tag"></i> <span>Planes</span></a></li>
            <li><a href="<?= base_url('adm/banners') ?>"><i class="fa fa-file-image-o"></i> <span>Banners</span></a></li>
			<li><a href="<?= base_url('adm/imagenes') ?>" target="_blank"><i class="fa fa-file-image-o"></i> <span>Administrador de Imágenes</span></a></li>
            <li><a href="<?= base_url('adm/configuraciones') ?>" ><i class="fa fa-gear"></i> <span>Configuraciones</span></a></li>
            <?php if ($this->auth->in_group('admin')): ?>
                <li><a href="<?= base_url('adm/usuarios') ?>"><i class="fa fa-users"></i> <span>Usuarios</span></a></li>
            <?php endif; ?>

            <?php if ($this->auth->in_group('admin')): ?>
                <li><a href="<?= base_url('adm/configuraciones-avanzadas') ?>"><i class="fa fa-gears"></i> <span>Configuraciones Avanzadas</span></a></li>
            <?php endif; ?>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

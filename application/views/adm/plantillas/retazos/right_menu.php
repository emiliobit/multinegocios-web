<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <span class="glyphicon glyphicon-user"></span>
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><?= $this->auth->get_user('name') . ' ' . $this->auth->get_user('last_name') ?></span>
            </a>
            <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header" style="height: auto">
                    <p>
                        <?= $this->auth->get_user('name') . ' ' . $this->auth->get_user('last_name') ?> - <?= $this->auth->get_user('email') ?>
                    </p>
                </li>
                <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-12 text-center">
                      <a href="<?= base_url('adm/perfil/cambiar-pass') ?>">Cambiar contraseña</a>
                    </div>
                  </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-left">
                        <a href="<?= base_url('adm/perfil/editar') ?>" class="btn btn-default btn-flat">Editar Perfil</a>
                    </div>
                    <div class="pull-right">
                        <a href="<?= base_url('adm/logout') ?>" class="btn btn-default btn-flat">Salir</a>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>
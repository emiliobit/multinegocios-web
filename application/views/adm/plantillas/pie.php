<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
</div><!--/.content-wrapper -->
<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <?= date('Y') ?>
    </div>
    <!-- Default to the left -->
    <strong><?= $config->nombre_sitio?></strong> | Desarrollado por <strong><a href="http://www.alta-resolucion.net" target="_blank">alta-resolucion.net</a></strong> &copy;. <?php date_default_timezone_set('America/Sao_Paulo'); echo date("Y") ?>.
</footer>
</div><!-- ./wrapper -->
</body>
</html>

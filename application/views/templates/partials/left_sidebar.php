<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Navegaci&oacute;n</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="<?= base_url() ?>"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
            <li><a href="<?= base_url('customers') ?>"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
            <li><a href="<?= base_url('patients') ?>"><i class="fa fa-stethoscope"></i> <span>Pacientes</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>M&aacute;s</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?= base_url('locations') ?>">Ciudades</a></li>
                    <li><a href="<?= base_url('species') ?>">Especies</a></li>
                    <li><a href="<?= base_url('breeds') ?>">Razas</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
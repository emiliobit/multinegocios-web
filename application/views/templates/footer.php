<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div><!--/.content-wrapper -->
<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Vet APP &copy; <?= date('Y') ?> <a href="#">Company</a>.</strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->
</body>
</html>



<?php if (isset($_categoria_items)): ?>
    <?php foreach ($_categoria_items as $_categoria_item): ?>
        <li class="dropdown">
			<a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_categoria_item->nombre_cat ?> <b class="caret"></b></a>
           <!-- <span><a href="#"><?php //echo $_categoria_item->nombre_cat ?><i class="icomoon-arrow-down-2"></i></a></span> -->
			
			
            <?php if (isset($_categoria_item->items)): ?>
           
                <ul class="dropdown-menu">
                    <?php foreach ($_categoria_item->items as $_item): ?>
                    <li><a href="<?php echo base_url('seccion/'.$_item->id_item.'/'.$_item->id_categoria_plan) ?>"><?php echo $_item->nombre_item ?> </a></li>
                    <?php endforeach; ?>
                </ul>
            
            <?php endif; ?>
			
        </li><!-- End Item -->
    <?php endforeach; ?>
<?php endif; ?>
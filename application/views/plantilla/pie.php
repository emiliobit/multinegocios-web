       <footer id="footer">
            <div class="container">
                <div class="col-md-3 col-sm-6">
                    <h4>About us</h4>

                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

                    <hr>

                    <h4>Join our monthly newsletter</h4>

                    <form>
                        <div class="input-group">

                            <input type="text" class="form-control">

                            <span class="input-group-btn">

                        <button class="btn btn-default" type="button"><i class="fa fa-send"></i></button>

                    </span>

                        </div>
                        <!-- /input-group -->
                    </form>

                    <hr class="hidden-md hidden-lg hidden-sm">

                </div>
                <!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">

                    <h4>Blog</h4>

                    <div class="blog-entries">
                        <div class="item same-height-row clearfix">
                            <div class="image same-height-always">
                                <a href="#">
                                    <img class="img-responsive" src="img/detailsquare.jpg" alt="">
                                </a>
                            </div>
                            <div class="name same-height-always">
                                <h5><a href="#">Blog post name</a></h5>
                            </div>
                        </div>

                        <div class="item same-height-row clearfix">
                            <div class="image same-height-always">
                                <a href="#">
                                    <img class="img-responsive" src="img/detailsquare.jpg" alt="">
                                </a>
                            </div>
                            <div class="name same-height-always">
                                <h5><a href="#">Blog post name</a></h5>
                            </div>
                        </div>

                        <div class="item same-height-row clearfix">
                            <div class="image same-height-always">
                                <a href="#">
                                    <img class="img-responsive" src="img/detailsquare.jpg" alt="">
                                </a>
                            </div>
                            <div class="name same-height-always">
                                <h5><a href="#">Very very long blog post name</a></h5>
                            </div>
                        </div>
                    </div>

                    <hr class="hidden-md hidden-lg">

                </div>
                <!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">

                    <h4>Contact</h4>

                    <p><strong>Universal Ltd.</strong>
                        <br>13/25 New Avenue
                        <br>Newtown upon River
                        <br>45Y 73J
                        <br>England
                        <br>
                        <strong>Great Britain</strong>
                    </p>

                    <a href="contact.html" class="btn btn-small btn-template-main">Go to contact page</a>

                    <hr class="hidden-md hidden-lg hidden-sm">

                </div>
                <!-- /.col-md-3 -->



                <div class="col-md-3 col-sm-6">

                    <h4>Photostream</h4>

                    <div class="photostream">
                        <div>
                            <a href="#">
                                <img src="<?php echo base_url(); ?>asset_2/img/detailsquare.jpg" class="img-responsive" alt="#">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="<?php echo base_url(); ?>asset_2/img/detailsquare2.jpg" class="img-responsive" alt="#">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="<?php echo base_url(); ?>asset_2/img/detailsquare3.jpg" class="img-responsive" alt="#">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="<?php echo base_url(); ?>asset_2/img/detailsquare3.jpg" class="img-responsive" alt="#">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="<?php echo base_url(); ?>asset_2/img/detailsquare2.jpg" class="img-responsive" alt="#">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="<?php echo base_url(); ?>asset_2/img/detailsquare.jpg" class="img-responsive" alt="#">
                            </a>
                        </div>
                    </div>

                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->

        <!-- *** COPYRIGHT ***
_________________________________________________________ -->

        <div id="copyright">
            <div class="container">
                <div class="col-md-12">
                    <p class="pull-left">&copy; <?php date_default_timezone_set('America/Sao_Paulo'); echo date("Y") ?>. <?= isset($items) ? $items->nombre_item . ' - ' . $config->nombre_sitio : $config->nombre_sitio ?></p>
                    <p class="pull-right">Desarrollado por <a href="http://www.alta-resolucion.net" target="_blank"> Alta-Resolucion.net</a></p>

                </div>
            </div>
        </div>
        <!-- /#copyright -->

        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->

    <!-- #### JAVASCRIPT FILES ### -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>asset_2/js/jquery.cookie.js"></script>
    <script src="<?php echo base_url(); ?>asset_2/js/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>asset_2/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>asset_2/js/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo base_url(); ?>asset_2/js/front.js"></script>

    

    <!-- owl carousel -->
    <script src="<?php echo base_url(); ?>asset_2/js/owl.carousel.min.js"></script>



</body>

<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-52d2faf72a1d37c8"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'dark',
    'share' : {
      'position' : 'right',
      'numPreferredServices' : 8
    }   
  });
</script>
<!-- AddThis Smart Layers END -->




<!-- WhatsHelp.io widget -->
<?php if(strlen($config->telefono1_sitio) > 0 ){$tel_whatsapp = $config->telefono1_sitio;} ?>
<script type="text/javascript">
	var tel_whatsapp;
	tel_whatsapp = <?php echo $tel_whatsapp ?>;
    (function () {
        var options = {
            facebook: "118336261542321", // Facebook page ID
            whatsapp: tel_whatsapp, // WhatsApp number
            company_logo_url: "//scontent.xx.fbcdn.net/v/t1.0-1/c14.0.171.171/s50x50/30808_118336354875645_5378943_n.jpg?oh=75da3fabc73295fd88df0c1e2e30b033&oe=5891D135", // URL of company logo (png, jpg, gif)
            greeting_message: "Hola, ¿cómo podemos ayudarle? Sólo tienes que enviar un mensaje ahora para obtener asistencia.", // Text of greeting message
            wa_vb_message: "Añadi el número a los contactos en tu teléfono y nos envía un mensaje a través de la aplicación.", // Message for WhatsApp or Viber
            button_color: "#FF0000", // Color of button
            position: "left", // Position may be 'right' or 'left'
            order: "facebook,whatsapp" // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
</html>
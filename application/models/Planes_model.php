<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Planes_model extends MY_Model
{

    public function __construct()
    {
        $this->table = 'planes';
        $this->primary_key = 'id_planes';
        parent::__construct();
    }
}

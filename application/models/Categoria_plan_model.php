<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categoria_plan_model extends MY_Model
{

    public function __construct()
    {
        $this->table = 'categoria_plan';
        $this->primary_key = 'id_categoria_plan';
        parent::__construct();
    }
}

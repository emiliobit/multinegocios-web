<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Item_model extends MY_model
{
    public function __construct()
    {
        $this->table = 'item';
        $this->primary_key = 'id_item';

        $this->has_one['categoria_item'] = array(
            'foreign_model' => 'Categoria_item_model',
            'foreign_table' => 'categoria_item',
            'foreign_key' => 'id_categoria_item', // La clave primaria de la otra tabla
            'local_key' => 'id_categoria_item', // La clave forenea en la tabla LOCAL
        );

        parent::__construct();
    }
}

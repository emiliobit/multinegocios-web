<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Session_model
 *
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Auto_login_model extends MY_Model {

    public function __construct() {
        $this->primary_key = 'id';
        $this->table = 'auto_login';
        $this->timestamps = FALSE;
        $this->before_create = array('hash_remember_code');
        $this->before_update = array('hash_remember_code');
        parent::__construct();
    }

    protected function hash_remember_code($data) {
        $data['remember_code'] = sha1($data['remember_code']);
        return $data;
    }

}


<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Configuracion_model extends MY_Model
{

    public function __construct()
    {
        $this->table = 'configuraciones';
        $this->primary_key = 'id_configuraciones';
        $this->timestamps = FALSE;
        $this->delete_cache_on_save = TRUE;
        parent::__construct();
    }

    public function get_conf($id_configuracion = 1)
    {
        $query = $this->db->get_where('configuraciones', array('id_configuraciones' => $id_configuracion));

        return $query->row_array();    //retorna la fila en un array
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Login_attempts
 *
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Login_attempt_model extends MY_Model {

    public function __construct() {
        $this->timestamps = FALSE;
        parent::__construct();
    }

}


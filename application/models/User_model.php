<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of User_model
 * @property Password_hasher $password_hasher
 * @author Sergio Ivan Alcaraz Cardozo
 */
class User_model extends MY_Model {

    public function __construct()
    {
        $this->table = 'users';
        $this->primary_key = 'id';

        $this->has_many['auto_login'] = array(
            'foreign_model' => 'Auto_login_model',
            'foreign_table' => 'auto_login',
            'foreign_key' => 'user_id',
            'local_key' => 'id');

        $this->has_many['sales'] = array(
            'foreign_model' => 'Sale_model',
            'foreign_table' => 'sales',
            'foreign_key' => 'user_id',
            'local_key' => 'id',
        );

        $this->before_create = array('hash_password');
        $this->before_update = array('hash_password', 'set_validation_code',
            'set_forgotten_pass_code', 'set_updated_at');

        $this->timestamps = FALSE;
        parent::__construct();
    }

    /**
     * Hash password for user in create and modified
     * @param array $data
     */
    protected function hash_password($data)
    {
        $this->load->library('password_hasher');
        if (isset($data['password'])) {
            $data['password'] = $this->password_hasher->hash($data['password']);
        }
        return $data;
    }

    /**
     * Set the validation code (HASH).
     * @param array $data
     * @return array $data
     */
    protected function set_validation_code($data)
    {
        if (isset($data['validation_code'])) {
            $data['validation_code'] = sha1($data['validation_code']);
        }
        return $data;
    }

    /**
     * Set the forgotten password code (HASH).
     * @param array $data
     * @return array $data
     */
    protected function set_forgotten_pass_code($data)
    {
        if (isset($data['forgotten_password_code'])) {
            $data['forgotten_password_code'] = sha1($data['forgotten_password_code']);
        }
        return $data;
    }

    /**
     * Set update_at value only if there is a "edited field".
     *
     * @param array $data
     * @return array
     */
    protected function set_updated_at($data)
    {
        $keys = array_keys($data);
        $intersect = array_intersect(array('name', 'last_name', 'password', 'username', 'email', 'group_name'), $keys);
        if (count($intersect) > 0) {
            $data['updated_at'] = date($this->timestamps_format);
        }
        return $data;
    }

}

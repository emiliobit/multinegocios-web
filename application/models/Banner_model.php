<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Banner_model extends MY_Model
{

    public function __construct()
    {
        $this->table = 'banner';
        $this->primary_key = 'id_banner';

        $this->has_one['item'] = array(
            'foreign_model' => 'Item_model',
            'foreign_table' => 'item',
            'foreign_key' => 'id_item', // La clave primaria de la otra tabla
            'local_key' => 'id_item', // La clave forenea en la tabla LOCAL
        );
        parent::__construct();
    }

}

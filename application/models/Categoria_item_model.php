<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Categoria_item_model extends MY_Model
{

    public function __construct()
    {
        $this->table = 'categoria_item';
        $this->primary_key = 'id_categoria_item';
        $this->has_many['items'] = array(
            'foreign_model' => 'Item_model',
            'foreign_table' => 'item',
            'foreign_key' => 'id_categoria_item',
            'local_key' => 'id_categoria_item',
        );

        parent::__construct();
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Error login
$lang['error_account_not_exists'] = 'Account does not exists';
$lang['error_account_inactive'] = 'Account is inactive';
$lang['error_password_incorrect'] = 'Password is incorrect';
$lang['error_not_logged'] = 'You have to Login';
$lang['error_not_authorized'] = 'You are not authorized to access that location';

// Track login attempts message
$lang['error_temporarily_locked'] = 'Temporarily Locked Out. Try again later';
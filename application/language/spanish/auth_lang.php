<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Error login
$lang['error_account_not_exists'] = 'La cuenta no existe';
$lang['error_account_inactive'] = 'La cuenta esta inactiva';
$lang['error_password_incorrect'] = 'La contraseña es incorrecta';
$lang['error_not_logged'] = 'Debes iniciar sesión';
$lang['error_not_authorized'] = 'No tiene permisos para acceder a ese lugar';

// Track login attempts message
$lang['error_temporarily_locked'] = 'Temporalmente bloqueado. Inténtalo de nuevo más tarde';
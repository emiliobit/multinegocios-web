<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
//    'customers' => array('authenticate' => array('index', 'view', 'create', 'update', 'delete', 'count_all')),
//    'users' => array('authenticate' => array('index', 'view', 'create', 'update', 'delete', 'count_all'), 'authorize' => 'admin'),
//    'payments' => array('authenticate' => array('index', 'history', 'enter_payment', 'rollback_payment')),
//    'auth' => array('authenticate' => array('user')),
//    'welcome' => array('authorize' => array('admin')),
    'adm_portada',
    'banners',
    'categoria_items',
    'categoria_planes',
    'config' => array('authenticate' => array('editar', 'editar_avanzado' => 'admin')),
    'items' => array('authenticate' => array('index', 'agregar_item', 'editar_item', 'eliminar_item', 'habilitado_item')),
    'planes' => array('authenticate' => array('index', 'agregar_plan', 'editar_plan', 'eliminar_plan', 'habilitado_plan')),
);

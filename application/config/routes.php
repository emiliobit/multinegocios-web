<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/*******************/
/*****BACKEND*******/
/*******************/
$route['adm/portada'] = 'adm_portada/index';

$route['adm/configuraciones'] = 'config/editar';
$route['adm/configuraciones-avanzadas'] = 'config/editar_avanzado';

$route['adm/planes'] = 'planes/index';
$route['adm/planes/agregar'] = 'planes/agregar_plan';
$route['adm/planes/editar/(:num)'] = 'planes/editar_plan/$1';
$route['adm/planes/eliminar/(:num)'] = 'planes/eliminar_plan/$1';

$route['adm/usuarios'] = 'usuarios/index';
$route['adm/usuarios/agregar'] = 'usuarios/agregar';
$route['adm/usuarios/editar/(:num)'] = 'usuarios/editar/$1';
$route['adm/usuarios/eliminar/(:num)'] = 'usuarios/eliminar/$1';
$route['adm/usuarios/habilitado/(:num)/(:num)'] = 'usuarios/habilitado/$1/$2';

$route['adm/items'] = 'items/index';
$route['adm/items/agregar'] = 'items/agregar_item';
$route['adm/items/editar/(:num)'] = 'items/editar_item/$1';
$route['adm/items/eliminar/(:num)'] = 'items/eliminar_item/$1';
$route['adm/items/habilitado/(:num)/(:num)'] = 'items/habilitado_item/$1/$2';

$route['adm/planes'] = 'planes/index';
$route['adm/planes/agregar'] = 'planes/agregar_plan';
$route['adm/planes/editar/(:num)'] = 'planes/editar_plan/$1';
$route['adm/planes/eliminar/(:num)'] = 'planes/eliminar_plan/$1';
$route['adm/planes/habilitado/(:num)/(:num)'] = 'planes/habilitado_plan/$1/$2';

$route['adm/categoria_planes'] = 'categoria_planes/index';
$route['adm/categoria_planes/agregar'] = 'categoria_planes/agregar_cat_plan';
$route['adm/categoria_planes/editar/(:num)'] = 'categoria_planes/editar_cat_plan/$1';
$route['adm/categoria_planes/eliminar/(:num)'] = 'categoria_planes/eliminar_cat_plan/$1';

$route['adm/categoria_items'] = 'categoria_items/index';
$route['adm/categoria_items/agregar'] = 'categoria_items/agregar_cat_item';
$route['adm/categoria_items/editar/(:num)'] = 'categoria_items/editar_cat_item/$1';
$route['adm/categoria_items/eliminar/(:num)'] = 'categoria_items/eliminar_cat_item/$1';
$route['adm/categoria_items/habilitado/(:num)/(:num)'] = 'categoria_items/habilitado_item/$1/$2';
$route['adm/categoria_items/destacado/(:num)/(:num)'] = 'categoria_items/habilitado_item/$1/$2';

$route['adm/banners'] = 'banners/index';
$route['adm/banners/agregar'] = 'banners/agregar';
$route['adm/banners/editar/(:num)'] = 'banners/editar/$1';
$route['adm/banners/eliminar/(:num)'] = 'banners/eliminar/$1';
$route['adm/banners/habilitado/(:num)/(:num)'] = 'banners/habilitado_banner/$1/$2';

$route['adm/imagenes'] = 'imagenes/index';
$route['adm/imagenes/agregar'] = 'imagenes/agregar';

$route['adm'] = 'adm_portada/index';

$route['adm/login'] = 'auth/login';
$route['adm/logout'] = 'auth/logout';

$route['adm/perfil/cambiar-pass'] = 'perfil/cambiar_password';
$route['adm/perfil/editar'] = 'perfil/editar';

/*******************/
/*****FRONTEND*******/
/*******************/

$route['seccion/(:num)/(:num)'] = 'items/view/$1/$2';

$route['plan/(:num)'] = 'planes/view/$1';
$route['plan/solicitar/(:num)'] = 'planes/solicitar/$1';

$route['default_controller'] = 'portada';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

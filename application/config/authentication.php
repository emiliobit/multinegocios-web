<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// Identity for user
$config['user']['identity'] = array('email');
// Remember me option
$config['allow_remember_me'] = TRUE;
// Cookie options for remember me (autologin)
$config['cookie']['remember_code'] = 'nde_r'; // Cookie remember code name
$config['cookie']['expire'] = 60 * 60 * 24 * 365; // Cookie by default expire in 1 year
$config['cookie']['random_string_type'] = 'alnum'; // random string type (remember me)
$config['cookie']['random_string_length'] = 40; // cookie random string length (remember me)
// Validation Account
$config['validation']['random_string_type'] = 'alnum'; // Type of random string
$config['validation']['random_string_length'] = 40;
$config['validation']['expire'] = 60 * 60 * 24; // Expire in a day
// Forgotten password
$config['forgotten_password']['random_string_type'] = 'alnum';
$config['forgotten_password']['random_string_length'] = 40;
$config['forgotten_password']['expire'] = 60 * 15;
// Login Attemps
$config['track_login_attempts'] = TRUE;
$config['max_attempts'] = 5; // Max attempts before start login delays
$config['second_delay'] = 60 * 2;
// Groups config
$config['groups'] = array('admin', 'staff', 'customer');
// Checking valid log
$config['next_check_second'] = 60 * 15;

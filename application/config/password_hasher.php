<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['algo'] = PASSWORD_BCRYPT;
$config['cost'] = 10;

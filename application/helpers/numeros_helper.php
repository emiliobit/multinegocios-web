<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	function NumFormat($num) {

		$separador_miles ='.'; 
		$separador_decimales = ','; 
		$cant_decimales = 0;
	
		if (empty($num))
			return 0;

		return number_format($num, $cant_decimales, $separador_decimales, $separador_miles);
	}
	


?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('check_is_unique')) {

    function check_is_unique($table, $field, $value, $primary_key = NULL, $primary_key_value = NULL)
    {
        $CI = &get_instance();
        $CI->load->database();
        if (!is_null($primary_key)) {
            $CI->db->where(array($primary_key . '<>' => $primary_key_value));
        }
        $CI->db->where(array($field => $value));

        return !(bool) $CI->db->count_all_results($table);
    }

}

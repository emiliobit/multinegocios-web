<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Menu_render
{

    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('categoria_item_model');
    }

    public function render_menu_cabecera_items()
    {
        $data['_categoria_items'] = $this->CI->categoria_item_model->with_items('where:`habilitado`=1')->get_all(array('habilitado' => TRUE));
        return $this->CI->load->view('plantilla/menu_cabecera_items', $data, TRUE);
    }
	
	public function render_menu_pie_items_empresa()
    {		
        $data['_categoria_items_pie'] = $this->CI->categoria_item_model->with_items('where:`id_categoria_item`= 1 and `habilitado`=1')->get_all(array('habilitado' => TRUE));
        return $this->CI->load->view('plantilla/menu_pie_items', $data, TRUE);
    }
	
	public function render_menu_pie_items_registro()
    {		
        $data['_categoria_items_pie_registro'] = $this->CI->categoria_item_model->with_items('where:`id_categoria_item`= 2 and `habilitado`=1')->get_all(array('habilitado' => TRUE));
        return $this->CI->load->view('plantilla/menu_pie_items_registro', $data, TRUE);
    }
	
	public function render_menu_pie_items_hosting()
    {		
        $data['_categoria_items_pie_hosting'] = $this->CI->categoria_item_model->with_items('where:`id_categoria_item`= 3 and `habilitado`=1')->get_all(array('habilitado' => TRUE));
        return $this->CI->load->view('plantilla/menu_pie_items_hosting', $data, TRUE);
    }
	
		public function render_menu_pie_items_pagos()
    {		
        $data['_categoria_items_pie_pagos'] = $this->CI->categoria_item_model->with_items('where:`id_categoria_item`= 4 and `habilitado`=1')->get_all(array('habilitado' => TRUE));
        return $this->CI->load->view('plantilla/menu_pie_items_pagos', $data, TRUE);
    }


}

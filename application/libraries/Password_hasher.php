<?php

/**
 * Wrapper for standard API password handling functions.
 *
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Password_hasher {

    /**
     * Algorithm using for hash
     * @var int
     */
    private $algo;

    /**
     * Algorithm cost
     * @var int
     */
    private $cost;

    /**
     * Access to codeigniter base controller.
     * @var CI_Controller
     */
    protected $CI;

    public function __construct($config = array())
    {
        $this->CI = & get_instance();
        $this->CI->load->config('password_hasher', TRUE);
        $this->initialize($config);
    }

    /**
     * Initialize configuration.
     * $config may contain 'algo' and 'cost' configuration.
     * @param array $config
     * @return boolean FALSE if is passed a incorrect config.
     */
    public function initialize($config = array())
    {
        $config_file = $this->CI->config->item('password_hasher');
        $config      = array_merge($config_file, $config);

        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Returns information about the given hash.
     *
     * @param string $hash
     * @return array
     */
    public function get_info($hash)
    {
        return password_get_info($hash);
    }

    /**
     * Creates a password hash.
     *
     * @param string $password
     * @return string
     */
    public function hash($password)
    {
        return password_hash($password, $this->algo, array('cost' => $this->cost));
    }

    /**
     * Checks if the given hash matches the given options.
     *
     * @param type $hash
     * @return boolean
     */
    public function need_rehash($hash)
    {
        return password_needs_rehash($hash, $this->algo, array('cost' => $this->cost));
    }

    /**
     * Verifies that a password matches a hash.
     *
     * @param string $password
     * @param string $hash
     * @return boolean TRUE if the password and hash match, or FALSE otherwise.
     */
    public function verify($password, $hash)
    {
        return password_verify($password, $hash);
    }

}

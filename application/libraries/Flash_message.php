<?php

/**
 * Flash message using bootstrap.
 *
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Flash_message {

    /**
     * CI instance
     * @var CI_Controller
     */
    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('session');
    }

    /**
     * Set flash message.
     *
     * @param string $message
     * @param string $level
     */
    private function set_message($message, $level) {
        $this->CI->session->set_flashdata('__flash_message', array('__message' => $message, '__level' => $level));
    }

    /**
     * Set success message. Green Alert
     *
     * @param string $message
     */
    public function success_message($message) {
        $this->set_message($message, 'success');
    }

    /**
     * Set warning message. Yellow Alert
     *
     * @param string $message
     */
    public function warning_message($message) {
        $this->set_message($message, 'warning');
    }

    /**
     * Set danger message. Red Alert
     *
     * @param string $message
     */
    public function danger_message($message) {
        $this->set_message($message, 'danger');
    }

    /**
     * Show message.
     *
     * @return string Boostrap alert. Must be echoed
     */
    public function show_message() {
        $flash_message = $this->CI->session->flashdata('__flash_message');
        if ($flash_message) {
            $message_rendered = $this->CI->load->view('flash_message/flash_message', $flash_message, TRUE);
            return $message_rendered;
        }
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Library for Authenticate user account.
 *
 * @property CI_Controller $CI
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Authentication
{

    /**
     * Codeigniter instance.
     * @var CI_Controller
     */
    private $CI;

    /**
     * Identity for user.
     * @var array
     */
    private $identity = array();

    /**
     * Cookie remember code name.
     * @var string
     */
    private $cookie_remember_code;

    /**
     * Time in second to expire cookie.
     * @var integer
     */
    private $cookie_remember_expire;

    /**
     * Allow autologin flag.
     * @var boolean
     */
    private $allow_remember_me;

    /**
     * Allow track login attempts flag.
     * @var boolean
     */
    private $track_login_attempts;

    /**
     * Next check time in second.
     * @var int
     */
    private $next_check_second;

    /**
     * Second delay
     * @var int
     */
    private $second_delay;

    /**
     * Max attempts
     * @var int
     */
    private $max_attempts;

    /**
     * User login attempts
     * @var NULL|stdClass
     */
    private $log_attempt = NULL;

    /**
     * Visitor ip address
     * @var string
     */
    private $ip_address = NULL;

    /**
     * Level Groups
     * @var array
     */
    public $groups;

    /**
     * Error message
     * @var string
     */
    public $error_message = '';

    /**
     * Successful message
     * @var string
     */
    public $success_message = '';

    public function __construct($config)
    {
        $this->CI = & get_instance();
        $this->CI->load->model(array('user_model', 'auto_login_model', 'login_attempt_model'));
        $this->CI->load->library(array('session', 'password_hasher'));
        $this->CI->load->helper(array('cookie', 'string', 'date'));
        $this->CI->load->config('authentication', TRUE);
        $this->CI->config->load('acl', TRUE);
        $this->CI->lang->load('auth');
        $this->initialize();

        $this->auto_login();
        $this->check_valid_login();
    }

    private function initialize()
    {
        $config = $this->CI->config->item('authentication');
        $this->identity = $config['user']['identity'];

        $this->cookie_remember_code = $config['cookie']['remember_code'];
        $this->cookie_remember_expire = $config['cookie']['expire'];
        $this->allow_remember_me = $config['allow_remember_me'];

        $this->track_login_attempts = $config['track_login_attempts'];
        $this->second_delay = $config['second_delay'];
        $this->max_attempts = $config['max_attempts'];

        $this->groups = $config['groups'];

        $this->next_check_second = $config['next_check_second'];

        if ($this->track_login_attempts) {
            $this->ip_address = $this->CI->input->ip_address();
        }
    }

    /**
     * Insert the $identity to an array with key matched with identity fields.
     * $array['username'] = 'foo'
     * $array['email'] = 'foo'
     * @param string $identity String of identity username, email, ...
     * @return array
     */
    private function _identity_to_array($identity)
    {
        foreach ($this->identity as $iden) {
            $array_iden[$iden] = $identity;
        }
        return $array_iden;
    }

    /**
     * Login a user
     * @param string $identity
     * @param string $password
     * @param boolean $remember
     * @return boolean TRUE if login success, FALSE otherwise
     */
    public function login($identity, $password, $remember = FALSE)
    {
        $user_identity = $this->_identity_to_array($identity);
        foreach ($user_identity as $field => $value) {
            $this->CI->user_model->where($field, '=', $value, TRUE);
        }
        $user = $this->CI->user_model->get();

        if (!$user) {
            $this->error_message = $this->CI->lang->line('error_account_not_exists');
            return FALSE;
        }

        if ($this->track_login_attempts) {
            $this->log_attempt = $this->CI->login_attempt_model->get(array('user_id' => $user->id, 'ip_address' => $this->ip_address));
        }

        if (!$this->can_login()) {
            $this->error_message = $this->CI->lang->line('error_temporarily_locked');
            return FALSE;
        }

        if (!$user->active) { //if user account is inactive
            $this->error_message = $this->CI->lang->line('error_account_inactive');
            return FALSE;
        }

        if (!$this->CI->password_hasher->verify($password, $user->password)) {
            $this->error_message = $this->CI->lang->line('error_password_incorrect');
            $this->failed_attempt($user->id);
            return FALSE;
        }
        // Here the user is login!!
        $user->next_check = $this->next_check_second + now();
        $this->CI->session->user = $user;
        if ($this->allow_remember_me) {
            $this->CI->session->user->remember_me = $remember;
        } else {
            $this->CI->session->user->remember_me = FALSE;
        }
        if ($remember) {
            $this->remember_user();
        }
        $this->successful_attempt();
        return TRUE;
    }

    /**
     * Logout the user account and destroy the session
     * @return boolean Return always true
     */
    public function logout()
    {
        if ($this->allow_remember_me) {
            $cookie_remember_code = get_cookie($this->cookie_remember_code);

            if ($cookie_remember_code) { // if there is the cookie then delete
                delete_cookie($this->cookie_remember_code);
                $this->CI->auto_login_model->delete(array('remember_code' => sha1($cookie_remember_code)));
            }
        }
        $this->CI->session->unset_userdata('user');
        $this->CI->session->sess_destroy();
        return TRUE;
    }

    /**
     * Check if is a user is already logged.
     *
     * @return bool TRUE if a user is logged in, FALSE otherwise.
     */
    public function logged_in()
    {
        return (bool) $this->CI->session->user;
    }

    /**
     * Get user property data.
     * @param null|string $property NULL for all user data, or string for a property
     * @return null|stdClass
     */
    public function get_user($property = NULL)
    {
        if (is_null($property)) {
            return $this->CI->session->user;
        }
        if ($this->CI->session->user) {
            return $this->CI->session->user->{$property};
        } else {
            return NULL;
        }
    }

    /**
     * Set user data
     * @param string $property
     * @param string $value
     * @return boolean
     */
    public function set_user($property, $value = NULL)
    {
        if (is_object($property)) {
            $this->CI->session->user = $property;
            return TRUE;
        }
        if ($this->CI->session->user) {
            $this->CI->session->user->{$property} = $value;
            return TRUE;
        }
        return FALSE;
    }

    /**
     * If remember me is allowed (autologin), It establishes the cookie and the remember_code.
     * @return NULL
     */
    public function remember_user()
    {
        if (!$this->allow_remember_me) {
            return;
        }
        $config = $this->CI->config->item('cookie', 'authentication');
        $cookie_string_type = $config['random_string_type'];
        $cookie_string_length = $config['random_string_length'];

        $id = $this->CI->session->user->id;
        do {
            $remember_code = random_string($cookie_string_type, $cookie_string_length);
            $remember_code_available = (bool) $this->CI->auto_login_model->fields('id')->get(array('remember_code' => sha1($remember_code)));
        } while ($remember_code_available);
        $previous_remember_code = get_cookie($this->cookie_remember_code);
        $expire = mdate('%Y-%m-%d %H:%i:%s', now() + $this->cookie_remember_expire);
        if ($previous_remember_code) {
            $previous_remember_code = sha1($previous_remember_code);
            // If there is a previous cookie update the remember_code with a new code
            if ($this->CI->auto_login_model->get(array('remember_code' => $previous_remember_code))) {
                $this->CI->auto_login_model->update(array('remember_code' => $remember_code, 'expire' => $expire), array('remember_code' => $previous_remember_code));
                set_cookie($this->cookie_remember_code, $remember_code, $this->cookie_remember_expire);
                return;
            }
        }
        // if no previous cookie create a new session
        $this->CI->auto_login_model->insert(array('user_id' => $id, 'remember_code' => $remember_code, 'expire' => $expire));
        set_cookie($this->cookie_remember_code, $remember_code, $this->cookie_remember_expire);
    }

    /**
     * Make auto login!!!
     */
    private function auto_login()
    {
        $cookie_remember_code = get_cookie($this->cookie_remember_code);

        if ($this->allow_remember_me && !$this->logged_in() && $cookie_remember_code) {
            $cookie_remember_code = sha1($cookie_remember_code);
            $session = $this->CI->auto_login_model->get(array('remember_code' => $cookie_remember_code));
            if (!$session || now() > human_to_unix($session->expire)) {
                return;
            }
            $user = $this->CI->user_model->get($session->user_id);
            if ($user && $user->active != 0) {
                $user->remember_me = TRUE;
                $user->next_check = 0;
                $this->CI->session->user = $user;
                $this->remember_user();
            }
        }
    }

    /**
     * Remove all sessions of the current user stored in DB and cookie
     * @param string|integer $id User id used to delete session, If NULL given delete all session of current user
     */
    public function destroy_all_auto_login()
    {
        if (!$this->logged_in()) {
            return;
        }
        $this->CI->auto_login_model->delete(array('user_id' => $this->CI->session->user->id));
    }

    /**
     * Check if the current login is valid. If in other session the user
     * change the password and this need to kick all other session with old password
     */
    private function check_valid_login()
    {
        if (!$this->logged_in() || $this->CI->session->user->next_check > now()) {
            return;
        }
        $user = $this->CI->user_model->get($this->CI->session->user->id);
        // If the password doesn't match or the user is inactive
        if (!$user || strcmp($this->CI->session->user->password, $user->password) !== 0 || $user->active == 0) {
            $this->logout();
        } else {
            $user->remember_me = $this->CI->session->user->remember_me;
            $user->next_check = $this->next_check_second + now();
            $this->CI->session->user = $user;
        }
    }

    /**
     * Relogin a already account logged
     */
    public function re_login()
    {
        if (!$this->logged_in()) {
            return;
        }
        $user = $this->CI->user_model->get($this->CI->session->user->id);
        if (!$user) {
            return;
        }
        $user->next_check = $this->next_check_second + now();
        $user->remember_me = $this->CI->session->user->remember_me;
        $this->CI->session->user = $user;
        if ($this->CI->session->user->remember_me) {
            $this->remember_user();
        }
    }

    /**
     * Generate a validation code for a given user.
     * @param integer $id user id
     * @return boolean|string return FALSE if don't generate the code, otherwise return the generated code
     */
    public function generate_validation_code($id)
    {
        $user = $this->CI->user_model->fields(array('id', 'validated', 'email'))->where(array('id' => $id, 'active <>' => 0))->get();
        if (!$user || $user->validated != 0) {
            return FALSE;
        }

        $config = $this->CI->config->item('validation', 'authentication');
        $validation_string_type = $config['random_string_type'];
        $validation_string_length = $config['random_string_length'];
        do {
            $validation_code = random_string($validation_string_type, $validation_string_length);
            $validation_code_available = (bool) $this->CI->user_model->where(array('validation_code' => sha1($validation_code)))->get();
        } while ($validation_code_available);

        $validation_code_expire = mdate('%Y-%m-%d %H:%i:%s', now() + $config['expire']);

        if ($this->CI->user_model->update(array('validation_code' => $validation_code, 'validation_code_expire' => $validation_code_expire), $user->id)) {
            return array('email' => sha1($user->email), 'validation_code' => $validation_code);
        }

        return FALSE;
    }

    /**
     * Check if the validation code is correct.
     * @param string $validation_code
     * @return boolean FALSE if the validation code does not exist, If
     * validation code corresponds to an inactive user,
     * If the user is already validated and if the validation code has expired,
     * otherwise TRUE.
     */
    public function check_validation_code($email, $validation_code)
    {
        $validation_code = sha1($validation_code);
        $user = $this->CI->user_model->fields(array('id', 'validation_code', 'validation_code_expire', 'email'))
                ->get(array('validation_code' => $validation_code));

        if (!$user || $user->active === 0 || $user->validated != 0 || strcmp($email, sha1($user->email)) || human_to_unix($user->validation_code_expire) < now()) {
            return FALSE;
        }

        $this->CI->user_model->update(array('validation_code' => NULL,
            'validation_code_expire' => NULL,
            'validated' => 1), $user->id);
        return TRUE;
    }

    /**
     * Generate save and return a code for forgotten password.
     * Return a associative array.
     * array('email' => sha1($email), 'forgotten_password_code' => $forgotten_password_code)
     * @param integer $id
     * @return boolean|array FALSE if there is no a user. Otherwise return the code.
     */
    public function generate_forgotten_password_code($id)
    {
        $user = $this->CI->user_model->fields(array('id', 'email'))->where(array('id' => $id, 'active <>' => 0))->get();

        if (!$user) {
            return FALSE;
        }
        $config = $this->CI->config->item('forgotten_password', 'authentication');
        $forg_pass_string_type = $config['random_string_type'];
        $forg_pass_string_length = $config['random_string_length'];
        do {
            $forgotten_password_code = random_string($forg_pass_string_type, $forg_pass_string_length);
            $code_available = (bool) $this->CI->user_model->where(array('forgotten_password_code' => sha1($forgotten_password_code)))->get();
        } while ($code_available);
        $forg_pass_expire = mdate('%Y-%m-%d %H:%i:%s', now() + $config['expire']);

        $this->CI->user_model->update(array('forgotten_password_code' => $forgotten_password_code,
            'forgotten_password_expire' => $forg_pass_expire), $user->id);
        return array('email' => sha1($user->email), 'forgotten_password_code' => $forgotten_password_code);
    }

    /**
     * Check if the code is correct. If is correct return TRUE and remove all
     * data related with forgotten password, otherwise return FALSE
     * @param string $forgotten_password_code
     * @return boolean
     */
    public function check_forgotten_password_code($email, $forgotten_password_code)
    {
        $user = $this->CI->user_model->fields(array('id', 'email', 'forgotten_password_code', 'forgotten_password_expire'))->get(array('forgotten_password_code' => sha1($forgotten_password_code)));

        if (!$user || is_null($user->forgotten_password_expire) || strcmp($email, sha1($user->email)) !== 0 || human_to_unix($user->forgotten_password_expire) < now()) {
            return FALSE;
        }

        $this->CI->user_model->update(array('forgotten_password_code' => NULL,
            'forgotten_password_expire' => NULL), $user->id);
        return TRUE;
    }

    /**
     * Check whether a user (looged) is in the group.
     * @param string|array $groups
     * @return boolean TRUE is the user is in the group, FALSE otherwise.
     */
    public function in_group($groups)
    {
        if (!$this->logged_in()) {
            return FALSE;
        }

        if (is_string($groups)) {
            $groups = array($groups);
        }

        return in_array($this->CI->session->user->group_name, $groups);
    }

    /**
     * Check if the user can login.
     * If the user does not exceed the number of attempts and if track_login_attempts
     * is set to FALSE, it return TRUE, otherwise return FALSE
     * @return boolean
     */
    private function can_login()
    {
        if (!$this->track_login_attempts) {
            return TRUE;
        }
        if (!$this->log_attempt) {
            return TRUE;
        }

        if (human_to_unix($this->log_attempt->next_attempt) < now()) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Register a failed attempt
     * @param int $id
     * @return type
     */
    private function failed_attempt($id)
    {
        if (!$this->track_login_attempts) {
            return;
        }
        if (!$this->log_attempt) {
            $this->CI->login_attempt_model->insert(array(
                'user_id' => $id,
                'ip_address' => $this->ip_address,
                'attempts' => 1,
                'last_attempt' => mdate('%Y-%m-%d %H:%i:%s'),
                'next_attempt' => NULL
            ));
        } else {
            $next_attempt = NULL;
            if (($this->log_attempt->attempts + 1) >= $this->max_attempts) {
                $next_attempt = mdate('%Y-%m-%d %H:%i:%s', now() + $this->second_delay);
            }
            $this->CI->login_attempt_model->update(array(
                'attempts' => $this->log_attempt->attempts + 1,
                'last_attempt' => mdate('%Y-%m-%d %H:%i:%s'),
                'next_attempt' => $next_attempt), $this->log_attempt->id);
        }
    }

    /**
     * Erase whether exists there a failed attempt
     */
    private function successful_attempt()
    {
        if (!$this->track_login_attempts) {
            return;
        }
        if ($this->log_attempt) {
            $this->CI->login_attempt_model->update(array(
                'attempts' => 0,
                'last_attempt' => mdate('%Y-%m-%d %H:%i:%s'),
                'next_attempt' => NULL), $this->log_attempt->id);
        }
    }

    /**
     * Authenticate user.
     */
    public function authenticate()
    {
        $acl = $this->CI->config->item('acl');
        $controller = $this->CI->router->class;
        $method = $this->CI->router->method;

        // Verify if is not necessary to authenticate
        if (!in_array($controller, $acl) && !isset($acl[$controller])) { // If controller is not listed
            return TRUE;
        } elseif (isset($acl[$controller]) && array_key_exists('authenticate', $acl[$controller]) && !in_array($method, $acl[$controller]['authenticate']) && !isset($acl[$controller]['authenticate'][$method])) {
            return TRUE; // The controller is listed but the method doesn't have to authenticate
        }

        // If is necessary authenticate
        if (!$this->logged_in()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Authorize access to a method it assumes that has already been authenticated a user
     */
    public function authorize()
    {
        $acl = $this->CI->config->item('acl');
        $controller = $this->CI->router->class;
        $method = $this->CI->router->method;
        $groups = NULL;

        // Verify if is not necessary to authorize
        if (!in_array($controller, $acl) && !isset($acl[$controller])) { // If controller not listed
            return TRUE;
        }

        if (isset($acl[$controller]['authenticate']) && array_key_exists($method, $acl[$controller]['authenticate'])) {
            // If a method have a specific authenticate
            $groups = $acl[$controller]['authenticate'][$method];
        } elseif (isset($acl[$controller]['authorize'])) {
            // If there is a general authorize
            // If implicit authorize all method then
            if (!isset($acl[$controller]['authenticate'])) {
                // No method listed in authenticate
                $groups = $acl[$controller]['authorize'];
            } elseif (in_array($method, $acl[$controller]['authenticate']) || array_key_exists($method, $acl[$controller]['authenticate'])) {
                // Method listed in authenticate
                $groups = $acl[$controller]['authorize'];
            }
        }

        // Lets authorize
        if (!is_null($groups) && !$this->in_group($groups)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

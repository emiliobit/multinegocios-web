<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Authentication $auth
 * @property Configuracion_model $configuracion_model
 * @property Flash_message $flash_message
 * @author Sergio Ivan Alcaraz Cardozo
 */
class MY_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('configuracion_model');
        $this->load->library(array('authentication' => 'auth', 'flash_message'));
        // nunca usar $this->config
        $this->config_sitio = $this->configuracion_model->set_cache('config_sitio')->get();
        if ($this->auth->authenticate() === FALSE) {
            $this->flash_message->danger_message('Por favor inicie sesión');
            redirect('adm/login');
        }

        if ($this->auth->authorize() === FALSE) {
            $this->flash_message->danger_message('No esta autorizado para entrar en esa sección');
            redirect('adm');
        }

        if ($this->config_sitio->estado_sitio == 0 && $this->auth->in_group('admin') === FALSE) {
            show_404();
        }
    }

}

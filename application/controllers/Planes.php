<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property Planes_model $planes_model
 */
class Planes extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('planes_model', 'categoria_plan_model','configuracion_model'));
    }

    public function index()
    {
        $data['planes'] = $this->planes_model->get_all();
		$data['config'] = $this->config_sitio;

        $data['css_files'] = array('assets/adminlte/plugins/datatables/dataTables.bootstrap.css', 'assets/adminlte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
        $data['js_files'] = array('assets/adminlte/plugins/datatables/jquery.dataTables.min.js', 'assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js', 'assets/adminlte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/planes', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function agregar_plan()
    {		
        if ($this->input->method() === 'post' && $this->form_validation->run('plan_agregar')) {
            $plan['nombre_plan'] = $this->input->post('nombre_plan');
            $plan['costo'] = $this->input->post('costo');
            $plan['espacio'] = $this->input->post('espacio');
            $plan['id_categoria_plan'] = $this->input->post('id_categoria_plan');
            $plan['descripcion_corta'] = $this->input->post('descripcion_corta');
            $plan['descripcion_detalle'] = $this->input->post('descripcion_detalle');
            $plan['habilitado'] = $this->input->post('habilitado');
            $plan['destacado'] = $this->input->post('destacado');
            $id = $this->planes_model->insert($plan);
            if ($id) {
                $this->flash_message->success_message('Se ha agregado el plan con éxito');
                redirect('adm/planes');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha agregado el plan');
            }
        }

        $data['css_files'] = array('assets/adminlte/plugins/select2/select2.min.css',);
        $data['js_files'] = array('assets/adminlte/plugins/select2/select2.full.min.js', 'assets/adminlte/plugins/ckeditor/ckeditor.js');

        $data['categorias'] = $this->categoria_plan_model->get_all();
		$data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/agregar_plan', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function editar_plan($id_planes)
    {
        $data['plan'] = $this->planes_model->get($id_planes);
        if ($this->input->method() === 'post' && $this->form_validation->run('plan_editar')) {
            $this->load->helper('validator');
            if (!check_is_unique($this->planes_model->table, 'nombre_plan', $this->input->post('nombre_plan'), $this->planes_model->primary_key, $id_planes)) {
                $data['form_error']['nombre_plan'] = 'Ya exite un plan con ese nombre. Intente con otro';
            }

            if (!isset($data['form_error'])) {
                $plan['nombre_plan'] = $this->input->post('nombre_plan');
                $plan['costo'] = $this->input->post('costo');
                $plan['espacio'] = $this->input->post('espacio');
                $plan['id_categoria_plan'] = $this->input->post('id_categoria_plan');
                $plan['descripcion_corta'] = $this->input->post('descripcion_corta');
                $plan['descripcion_detalle'] = $this->input->post('descripcion_detalle');
                $plan['habilitado'] = $this->input->post('habilitado');
                $plan['destacado'] = $this->input->post('destacado');
                $id = $this->planes_model->update($plan, $id_planes);
                if ($id) {
                    $this->flash_message->success_message('Se ha editado el plan con éxito');
                    redirect('adm/planes');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha editado el plan');
                }
            }
        }

        $data['categorias'] = $this->categoria_plan_model->get_all();
		$data['config'] = $this->config_sitio;

        $data['css_files'] = array('assets/adminlte/plugins/select2/select2.min.css',);
        $data['js_files'] = array('assets/adminlte/plugins/select2/select2.full.min.js', 'assets/adminlte/plugins/ckeditor/ckeditor.js');

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/editar_plan', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function eliminar_plan($id_planes)
    {
        if ($this->input->method() === 'post') {
            $eliminado = $this->planes_model->delete($id_planes);
            if ($eliminado) {
                $this->flash_message->success_message('Se ha eliminado el plan con éxito');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha eliminado el plan');
            }
            redirect('adm/planes');
        }
    }

    public function view($id_planes)
    {
        $data['plan_dato'] = $this->planes_model->get(array('id_planes' => $id_planes));
        $data['config'] = $this->configuracion_model->get();

        //trae los planes asociados al item, si los hubiere
        //$data['planes'] = $this->planes_model->get_all(array('habilitado' => 1, 'id_categoria_plan' => $id_categoria_plan ));

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/plan', $data);
        $this->load->view('plantilla/pie');
    }

    public function habilitado_plan($id_planes, $habilitado)
    {
        if ($habilitado == 0) {
            $plan['habilitado'] = 1;
        } else {
            $plan['habilitado'] = 0;
        }
        $id = $this->planes_model->update($plan, $id_planes);

        if ($id) {
            if ($plan['habilitado'] == 1) {
                $this->flash_message->success_message('Plan habilitado');
            } else {
                $this->flash_message->success_message('Plan deshabilitado');
            }
        } else {
            $this->flash_message->danger_message('Ocurrio un error. Intente de nuevo');
        }

        redirect('adm/planes');
    }
	
	public function solicitar($id_planes)
    {
		 $data['config'] = $this->config_sitio;	
		 $this->load->library('form_validation');

        if ($this->input->post('solicitud_form') && $this->form_validation->run('solicitar_plan')) {
            $registro = $this->input->post();
            $this->load->library('email');

            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $this->email->initialize($config);

            $this->email->from($registro['email'], $registro['nombre'] . ' ' . $registro['apellido']);
            $this->email->to('emiliosotto@gmail.com');
			//$this->email->to($registro['sitio_email']);
            $this->email->subject('Solicitud de plan de hospedaje: ' . $registro['plan']);
			
            $mensaje = '<p>Nombre de solicitante: ' . $registro['nombre'] . ' ' . $registro['apellido'] . '</p>';
			$mensaje .= '<p>Empresa: ' . $registro['empresa'] . '</p>';
            $mensaje .= '<p>Cedula: ' . $registro['cedula'] . '</p>';
			$mensaje .= '<p>RUC: ' . $registro['ruc'] . '</p>';
            $mensaje .= '<p>Email: ' . $registro['email'] . '</p>';            
            $mensaje .= '<p>Nro. de fax: ' . $registro['fax'] . '</p>';
            $mensaje .= '<p>Nro. de telefono: ' . $registro['telefono'] . '</p>';            
            $mensaje .= '<p>Dirección: ' . $registro['direccion'] . '</p>';
            $mensaje .= '<p>Ciudad: ' . $registro['ciudad'] . '</p>';  
			$mensaje .= '<p>Direccion IP: ' . $registro['ip'] . '</p>';			
			
            $this->email->message($mensaje);
			/*
			echo $this->email->print_debugger(array('headers'));
			echo $this->email->print_debugger(array('subject'));
			echo $this->email->print_debugger(array('body'));
			*/
			
			//echo $this->email->display_errors();
			
            $enviado = $this->email->send();

            if ($enviado) {
                $this->load->view('plantilla/cabecera', $data);
                $this->load->view('paginas/email_enviado', $data);
                $this->load->view('plantilla/pie');
                return;
            } else {
                $data['error_envio'] = TRUE;
            }
        }

        $this->load->model('planes_model');
		
		$data['plan_dato'] = $this->planes_model->get(array('id_planes' => $id_planes));
        

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/form_planes', $data);
        $this->load->view('plantilla/pie');
		
	}

}

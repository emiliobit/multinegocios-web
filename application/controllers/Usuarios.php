<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property User_model $user_model
 */
class Usuarios extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'flash_message'));
        $this->load->model(array('User_model'));

        if ($this->auth->in_group('admin')) {
            $this->flash_message->warning_message('No esta autorizado para acceder');
            redirect('adm');
        }
    }

    public function index()
    {
        $data['usuarios'] = $this->user_model->get_all();

        $data['css_files'] = array('assets/adminlte/plugins/datatables/dataTables.bootstrap.css', 'assets/adminlte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
        $data['js_files'] = array('assets/adminlte/plugins/datatables/jquery.dataTables.min.js', 'assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js', 'assets/adminlte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');

        $data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/usuarios', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function agregar()
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('usuarios_agregar')) {
            $usuario['email'] = $this->input->post('email');
            $usuario['name'] = $this->input->post('name');
            $usuario['last_name'] = $this->input->post('last_name');
            $usuario['password'] = $this->input->post('password');
            $usuario['active'] = $this->input->post('active');
            $usuario['group_name'] = $this->input->post('group_name');

            $id = $this->user_model->insert($usuario);

            if ($id) {
                $this->flash_message->success_message('Se ha agregado el usuario con éxito');
                redirect('adm/usuarios');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha agregado el usuario');
            }
        }

        $data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/agregar_usuario', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function editar($id_usuario)
    {
        $data['usuario'] = $this->user_model->get($id_usuario);
        if ($this->input->method() === 'post' && $this->form_validation->run('usuarios_editar')) {
            $this->load->helper('validator');
            if (!check_is_unique($this->user_model->table, 'email', $this->input->post('email'), $this->user_model->primary_key, $id_usuario)) {
                $data['form_error']['email'] = 'Ya exite un plan con ese nombre. Intente con otro';
            }

            if (!isset($data['form_error']['email'])) {
                $usuario['email'] = $this->input->post('email');
                $usuario['name'] = $this->input->post('name');
                $usuario['last_name'] = $this->input->post('last_name');
                if ($this->input->post('password') !== '') {
                    $usuario['password'] = $this->input->post('password');
                }
                $usuario['active'] = $this->input->post('active');
                $usuario['group_name'] = $this->input->post('group_name');
                $id = $this->user_model->update($usuario, $id_usuario);

                if ($id) {
                    $this->flash_message->success_message('Se ha editado el usuario con éxito');
                    redirect('adm/usuarios');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha editado el usuario');
                }
            }
        }

        $data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/editar_usuario', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function eliminar($id_usuario)
    {
        if ($this->input->method() === 'post') {
            $eliminado = $this->user_model->delete($id_usuario);

            if ($eliminado) {
                $this->flash_message->success_message('Se ha eliminado el usuario con éxito');
                redirect('adm/usuarios');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha eliminado el usuario');
            }
        }
    }

    public function habilitado($id_usuario, $active)
    {
        if ($active == 0) {
            $usuario['active'] = 1;
        } else {
            $usuario['active'] = 0;
        }
        $id = $this->user_model->update($usuario, $id_usuario);
        if ($id) {
            if ($usuario['active'] == 1) {
                $this->flash_message->success_message('Usuario habilitado');
            } else {
                $this->flash_message->success_message('Usuario deshabilitado');
            }
        } else {
            $this->flash_message->danger_message('Ocurrio un error. Intente de nuevo');
        }
        redirect('adm/usuarios');
    }
}

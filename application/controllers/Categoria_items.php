<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @property Categoria_item_model $categoria_item_model
 * @property Flash_message $flash_message
 */
class Categoria_items extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'flash_message'));
        $this->load->model('categoria_item_model','configuracion_model');
    }

    public function index()
    {
        //$data['categoria_item'] = $this->categoria_item_model->get_categoria_item();
        $data['categoria_item'] = $this->categoria_item_model->get_all();		$data['config'] = $this->config_sitio;
        $data['css_files'] = array('assets/adminlte/plugins/datatables/dataTables.bootstrap.css', 'assets/adminlte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
        $data['js_files'] = array('assets/adminlte/plugins/datatables/jquery.dataTables.min.js', 'assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js', 'assets/adminlte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/categoria_items', $data);
        $this->load->view('adm/plantillas/pie');
    }
    public function agregar_cat_item()
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('categoria_item_agregar')) {
            $categoria_item['nombre_cat'] = $this->input->post('nombre_cat');
            $categoria_item['descripcion'] = $this->input->post('descripcion');
            $categoria_item['habilitado'] = $this->input->post('habilitado');
            $id = $this->categoria_item_model->insert($categoria_item);
            if ($id) {
                $this->flash_message->success_message('Se ha agregado la categoria con éxito');
                redirect('adm/categoria_items');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha agregado la categoria');
            }
        }				$data['config'] = $this->config_sitio;
        $this->load->view('adm/plantillas/cabecera');
        $this->load->view('adm/paginas/agregar_categoria_items');
        $this->load->view('adm/plantillas/pie');
    }
    public function editar_cat_item($id_categoria_item)
    {
        $data['categoria_item'] = $this->categoria_item_model->get($id_categoria_item);
        if ($this->input->method() === 'post' && $this->form_validation->run('categoria_item_editar')) {
            $this->load->helper('validator');
            if (!check_is_unique($this->categoria_item_model->table, 'nombre_cat', $this->input->post('nombre_cat'), $this->categoria_item_model->primary_key, $id_categoria_item)) {
                $data['form_error']['nombre_cat'] = 'Ya exite una categoria con ese nombre. Intente con otro';
            }
            if (!isset($data['form_error'])) {
                $categoria_item['nombre_cat'] = $this->input->post('nombre_cat');
                $categoria_item['descripcion'] = $this->input->post('descripcion');
                $categoria_item['habilitado'] = $this->input->post('habilitado');
                $id = $this->categoria_item_model->update($categoria_item, $id_categoria_item);
                if ($id) {
                    $this->flash_message->success_message('Se ha editado la categoria con éxito');
                    redirect('adm/categoria_items');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha editado la categoria');
                }
                redirect('adm/categoria_items');
            }
        }
		$data['config'] = $this->config_sitio;
        $this->load->view('adm/plantillas/cabecera');
        $this->load->view('adm/paginas/editar_categoria_items', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function habilitado_item($id_categoria_item, $habilitado)
    {
        if ($habilitado == 0) {
            $item['habilitado'] = 1;
        } else {
            $item['habilitado'] = 0;
        }
        $id = $this->categoria_item_model->update($item, $id_categoria_item);
        if ($id) {
            if ($item['habilitado'] == 1) {
                $this->flash_message->success_message('Categoria habilitado');
            } else {
                $this->flash_message->success_message('Categoria deshabilitado');
            }
        } else {
            $this->flash_message->danger_message('Ocurrio un error. Intente de nuevo');
        }
        redirect('adm/categoria_items');
    }
    public function eliminar_cat_item($id_categoria_item)
    {
        if ($this->input->method() === 'post') {
            $this->load->model('item_model');
            $count_items = $this->item_model->count_rows(array('id_categoria_item' => $id_categoria_item));

            // Si se puede eliminar. Check foreign key
            if ($count_items == 0) {
                $eliminado = $this->categoria_item_model->delete($id_categoria_item);
                if ($eliminado) {
                    $this->flash_message->success_message('Se ha eliminado la categoria con éxito');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha eliminado la categoria');
                }
            } else {
                $this->flash_message->danger_message('No se puede eliminar la categoria, existen Items que dependen de la categoria');
            }
            redirect('adm/categoria_items');
        }
    }
}

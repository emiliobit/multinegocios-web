<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Auth
 *
 * @property Flash_message $flash_message
 * @property Authentication $auth
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('authentication' => 'auth', 'flash_message', 'form_validation'));
    }

    public function login()
    {
        $data['config'] = $this->configuracion_model->set_cache('config_sitio')->get();
        if ($this->input->method() === 'post' && $this->form_validation->run('login')) {
            $login = $this->auth->login($this->input->post('email'), $this->input->post('password'), $this->input->post('remember_me'));
            if ($login) {
                $this->flash_message->success_message('Bienvenido ' . $this->auth->get_user('name') . ' ' . $this->auth->get_user('last_name'));
                redirect('adm');
            } else {
                $data['error'] = $this->auth->error_message;
            }
        }
        $this->load->view('adm/plantillas/login', $data);
    }

    public function logout()
    {
        $this->auth->logout();
        redirect('adm/login');
    }

}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Whois extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
		//$this->load->model(array('planes_model','banner_model'));		
    }
	
	
	public function registrar_privado()	{
		
        if ($this->input->method() !== 'post') {
            redirect(base_url());
        }
        $data['config'] = $this->config_sitio;
        $data['dominio'] = $this->input->post('dominio');

        $this->load->library('form_validation');

        if ($this->input->post('solicitud_whois_priv') ) {
            $registro = $this->input->post();
            $this->load->library('email');

            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $this->email->initialize($config);

            $this->email->from($registro['email'], $registro['nombre'] . ' ' . $registro['apellido']);
            $this->email->to('emiliosotto@gmail.com');

            $this->email->subject('Solicitud Whois Privado de dominio: ' . $registro['dominio']. $registro['tld']);
            $mensaje = '<p>Nombre de solicitante: ' . $registro['nombre'] . ' ' . $registro['apellido'] . '</p>';
            $mensaje .= '<p>Email: ' . $registro['email'] . '</p>';
            $mensaje .= '<p>Numero de telefono: ' . $registro['telefono'] . '</p>';            
            $this->email->message($mensaje);
            $enviado = $this->email->send();

            if ($enviado) {
                $this->load->view('plantilla/cabecera', $data);
                $this->load->view('paginas/email_enviado', $data);
                $this->load->view('plantilla/pie');
                return;
            } else {
                $data['error_envio'] = TRUE;
            }
        }

       // $this->load->model('planes_model');

       // $data['planes'] = $this->planes_model->get_all(array('id_categoria_plan' => $data['config']->plan_reg_dominio, 'habilitado' => TRUE));

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/form/form_whois_privado', $data);
        $this->load->view('plantilla/pie');
    }

    public function consultar()
    {		
				
		if ($this->input->method() === 'post') {		
			$dominio = $this->input->post('dominio');
			redirect('http://whois.domaintools.com/'.$dominio);
		}
    }
}

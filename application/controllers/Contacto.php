<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        //$this->load->helper('captcha');
    }

    public function index() {

		$data['config'] = $this->config_sitio;
        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/contacto', $data);
        $this->load->view('plantilla/pie', $data);
    }
	

    public function enviar() {
	
		if ($this->input->method() !== 'post') {
            redirect(base_url());			
        }
		$data['config'] = $this->config_sitio;
	
        $this->load->library('form_validation');
	
        if ($this->input->post('reg_form') && $this->form_validation->run('contacto')) {
		
            $registro = $this->input->post();
            $this->load->library('email');
	
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
			
			//$data['sitio_email'] = $this->input->post('sitio_email');			
			
            $this->email->initialize($config);
            $this->email->from($registro['email'], $registro['nombre'] );
            $this->email->to($registro['email_sitio']);
            $this->email->subject('Desde la web: ' . $registro['asunto']);
			
            $mensaje = '<p>Nombre: ' . $registro['nombre']  . '</p>';            
            $mensaje .= '<p>Email: ' . $registro['email'] . '</p>';
            $mensaje .= '<p>Numero de telefono: ' . $registro['telefono'] . '</p>';
            $mensaje .= '<p>Ciudad: ' . $registro['ciudad'] . '</p>';
            $mensaje .= '<p>Mensaje: ' . $registro['mensaje'] . '</p>';           
			
            $this->email->message($mensaje);
            $enviado = $this->email->send();

            if ($enviado) {
			
                $this->load->view('plantilla/cabecera', $data);
                $this->load->view('paginas/email_enviado', $data);
                $this->load->view('plantilla/pie');				
                return;
            } else {
                $data['error_envio'] = TRUE;
				
            }
        }
		
		$this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/contacto', $data);
        $this->load->view('plantilla/pie', $data);

    }

}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Portada extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->model('planes_model');
		$this->load->model('banner_model');
    }

    public function index()
    {
        $data['config'] = $this->configuracion_model->get();
		
		//trae los planes destacados, si los hubiere	
		$data['planes_destacados'] = $this->planes_model->get_all(array('habilitado' => 1, 'destacado' => 1 ));
		$data['banner'] = $this->banner_model->order_by('orden', $order = 'ASC')->get_all(array('habilitado' => 1 ));

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/portada', $data);
        $this->load->view('plantilla/pie');
    }	
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Categoria_plan_model $categoria_plan_model
 */
class Categoria_planes extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model(array('categoria_plan_model','configuracion_model'));
        $this->load->library(array('form_validation', 'flash_message'));
    }

    public function index()
    {

        $data['categoria_planes'] = $this->categoria_plan_model->get_all();
		$data['config'] = $this->config_sitio;

        $data['css_files'] = array('assets/adminlte/plugins/datatables/dataTables.bootstrap.css', 'assets/adminlte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
        $data['js_files'] = array('assets/adminlte/plugins/datatables/jquery.dataTables.min.js', 'assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js', 'assets/adminlte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/categoria_planes', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function agregar_cat_plan()
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('categoria_plan_agregar')) {
            $categoria_plan['nombre_plan'] = $this->input->post('nombre_plan');
            $categoria_plan['detalle'] = $this->input->post('detalle');
            $categoria_plan['habilitado'] = $this->input->post('habilitado');
            $categoria_plan['destacado'] = $this->input->post('destacado');
            $id = $this->categoria_plan_model->insert($categoria_plan);
            if ($id) {
                $this->flash_message->success_message('Se ha agregado el tipo de plan con éxito');
                redirect('adm/categoria_planes');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha agregado el tipo de plan');
            }
        }

        $data['js_files'] = array('assets/adminlte/plugins/ckeditor/ckeditor.js');
		$data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/agregar_categoria_planes');
        $this->load->view('adm/plantillas/pie');
    }

    public function editar_cat_plan($id_categoria_plan)
    {
        $data['categoria_plan'] = $this->categoria_plan_model->get($id_categoria_plan);
        if ($this->input->method() === 'post' && $this->form_validation->run('categoria_plan_editar')) {
            $this->load->helper('validator');
            if (!check_is_unique($this->categoria_plan_model->table, 'nombre_plan', $this->input->post('nombre_plan'), $this->categoria_plan_model->primary_key, $id_categoria_plan)) {
                $data['form_error']['nombre_plan'] = 'Ya exite un tipo de plan con ese nombre. Intente con otro';
            }

            if (!isset($data['form_error'])) {
                $item['nombre_plan'] = $this->input->post('nombre_plan');
                $item['detalle'] = $this->input->post('detalle');
                $item['habilitado'] = $this->input->post('habilitado');
                $item['destacado'] = $this->input->post('destacado');
                $id = $this->categoria_plan_model->update($item, $id_categoria_plan);
                if ($id) {
                    $this->flash_message->success_message('Se ha editado el tipo de plan con éxito');
                    redirect('adm/categoria_planes');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha editado el tipo de plan');
                }
            }
        }

        $data['js_files'] = array('assets/adminlte/plugins/ckeditor/ckeditor.js');
		$data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/editar_categoria_plan', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function habilitado_plan($id_categoria_plan, $habilitado)
    {
        if ($habilitado == 0) {
            $item['habilitado'] = 1;
        } else {
            $item['habilitado'] = 0;
        }
        $id = $this->categoria_plan_model->update($item, $id_categoria_plan);
        if ($id) {
            if ($item['habilitado'] == 1) {
                $this->flash_message->success_message('Tipo de plan habilitado');
            } else {
                $this->flash_message->success_message('Tipo de plan deshabilitado');
            }
        } else {
            $this->flash_message->danger_message('Ocurrio un error. Intente de nuevo');
        }
        redirect('adm/categoria_planes');
    }

    public function eliminar_cat_plan($id_categoria_plan)
    {
        if ($this->input->method() === 'post') {
            $this->load->model('planes_model');
            $count_items = $this->planes_model->count_rows(array('id_categoria_plan' => $id_categoria_plan));

            // Si se puede eliminar. Check foreign key
            if ($count_items == 0) {
                $eliminado = $this->categoria_plan_model->delete($id_categoria_plan);
                if ($eliminado) {
                    $this->flash_message->success_message('Se ha eliminado el tipo de plan con éxito');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha eliminado el tipo de plan');
                }
            } else {
                $this->flash_message->danger_message('No se puede eliminar el tipo de plan, existen planes que dependen de el tipo de plan');
            }
            redirect('adm/categoria_planes');
        }
    }

}

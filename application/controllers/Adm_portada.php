<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Adm_portada extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('item_model','planes_model','categoria_item_model', 'categoria_plan_model','configuracion_model'));
    }

    public function index()
    {
		$data['items_cant'] = $this->item_model->count_rows();
		$data['planes_cant'] = $this->planes_model->count_rows();
		$data['cat_plan_cant'] = $this->categoria_plan_model->count_rows();
		$data['cat_item_cant'] = $this->categoria_item_model->count_rows();
		$data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/portada', $data);
        $this->load->view('adm/plantillas/pie');
    }

}

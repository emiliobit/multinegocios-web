<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Banner_model $banner_model
 * @property Item_model $item_model
 * @property Flash_message $flash_message
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Banners extends MY_Controller
{

    private $upload_config;
    private $upload_messages = array(
        'upload_file_exceeds_limit' => 'El archivo subido excede el tamaño máximo permitido: 1MB',
        'upload_invalid_filesize' => 'El archivo subido excede el tamaño máximo permitido: 1MB',
        'upload_invalid_dimensions' => 'La imagen excede la dimensión maxima permitida 1024x780',
        'upload_invalid_filetype' => 'El tipo de archivo no esta permitido, se admiten jpg, png y gif',
        'upload_no_file_selected' => 'No seleccionaste el banner a subir',
    );

    public function __construct()
    {
        parent::__construct();
        $this->upload_config = array(
            'upload_path' => FCPATH . 'uploads/banners/',
            'allowed_types' => 'jpg|jpeg|png',
            'encrypt_name' => TRUE,
            'max_size' => '1024',
        );
        $this->load->model(array('banner_model','configuracion_model'));
        $this->load->library(array('form_validation', 'flash_message'));
        $this->lang->load('upload');
        $this->lang->language = array_merge($this->lang->language, $this->upload_messages);
    }

    public function index()
    {
        $data['banners'] = $this->banner_model->get_all();
		$data['config'] = $this->config_sitio;

        $data['css_files'] = array('assets/adminlte/plugins/datatables/dataTables.bootstrap.css', 'assets/adminlte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
        $data['js_files'] = array('assets/adminlte/plugins/datatables/jquery.dataTables.min.js', 'assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js', 'assets/adminlte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/banners', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function agregar()
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('banner')) {
            $this->load->library('upload', $this->upload_config);
            if ($this->upload->do_upload('imagen')) {
                $banner['tipo'] = 0;
                $banner['texto1'] = $this->input->post('texto1');
                $banner['texto2'] = $this->input->post('texto2');
                $banner['texto3'] = $this->input->post('texto3');
                $banner['orden'] = $this->input->post('orden');
                $banner['id_item'] = NULL;
                $banner['habilitado'] = $this->input->post('habilitado');
                $banner['imagen'] = $this->upload->data('file_name');

                $id = $this->banner_model->insert($banner);
                if ($id) {
                    $this->flash_message->success_message('Se ha agregado el banner con éxito');
                    redirect('adm/banners');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha agregado el banner');
                }
            } else {
                $this->flash_message->danger_message($this->upload->display_errors('', '<br>'));
            }
        }

		$data['config'] = $this->config_sitio;
		
        $this->load->view('adm/plantillas/cabecera');
        $this->load->view('adm/paginas/agregar_banner');
        $this->load->view('adm/plantillas/pie');
    }

    public function editar($id_banner)
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('banner')) {

            if (is_uploaded_file($_FILES['imagen_cambiar']['tmp_name'])) {
                $this->load->library('upload', $this->upload_config);
                if ($this->upload->do_upload('imagen_cambiar')) {
                    $banner_anterior = $this->banner_model->fields(array('imagen'))->get($id_banner);
                    unlink(FCPATH . 'uploads/banners/' . $banner_anterior->imagen);
                    $banner['imagen'] = $this->upload->data('file_name');
                } else {
                    $error_banner = 'Ocurrio un error al intentar guardar la imagen. Por favor intente de nuevo';
                }
            }
            $banner['texto1'] = $this->input->post('texto1');
            $banner['texto2'] = $this->input->post('texto2');
            $banner['texto3'] = $this->input->post('texto3');
            $banner['orden'] = $this->input->post('orden');
            $banner['habilitado'] = $this->input->post('habilitado');

            $id = $this->banner_model->update($banner, $id_banner);

            if ($id) {
                if (isset($error_banner)) {
                    $mensaje = 'Se ha editadto el banner con éxito pero ocurrio un error al guardar la imagen.';
                } else {
                    $mensaje = 'Se ha editado el banner con éxito';
                }
                $this->flash_message->success_message($mensaje);
                redirect('adm/banners');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha agregado el banner');
            }
        }

        $data['banner'] = $this->banner_model->get($id_banner);
		$data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera');
        $this->load->view('adm/paginas/editar_banner', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function eliminar($id_banner)
    {
        if ($this->input->method() === 'post') {
            $banner = $this->banner_model->get($id_banner);
            unlink(FCPATH . 'uploads/banners/' . $banner->imagen);
            $eliminado = $this->banner_model->delete($id_banner);
            if ($eliminado) {
                $this->flash_message->success_message('Se ha eliminado el banner con éxito');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha eliminado el banner');
            }
            redirect('adm/banners');
        }
    }

    public function habilitado_banner($id_banner, $habilitado)
    {
        if ($habilitado == 0) {
            $banner['habilitado'] = 1;
        } else {
            $banner['habilitado'] = 0;
        }

        $id = $this->banner_model->update($banner, $id_banner);

        if ($id) {
            if ($banner['habilitado'] == 1) {
                $this->flash_message->success_message('Banner habilitado');
            } else {
                $this->flash_message->success_message('Banner deshabilitado');
            }
        } else {
            $this->flash_message->danger_message('Ocurrio un error. Intente de nuevo');
        }

        redirect('adm/banners');
    }

}

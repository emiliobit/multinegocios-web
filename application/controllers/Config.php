<?php

/**
 * @property Configuracion_model $configuracion_model
 * @property Flash_message $flash_message
 * @property Categoria_plan_model $categoria_plan_model
 */
class Config extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');

        $this->load->library(array('form_validation', 'flash_message'));
        $this->load->model(array('configuracion_model', 'categoria_plan_model'));
    }

    private function index()
    {
        $data['config'] = $this->config_model->as_array()->get();

        $this->load->view('paginas/configuraciones', $data);
    }

    public function editar($id_config = 1)
    {
        //$data = $this->configuracion_model->get($id_config);
        $data['config'] = $this->config_sitio;

        if ($this->input->method() === 'post' && $this->form_validation->run('configuracion_sitio')) {
            $config['nombre_sitio'] = $this->input->post('nombre_sitio');
            $config['descripcion_sitio'] = $this->input->post('descripcion_sitio');
            $config['email_sitio'] = $this->input->post('email_sitio');
            $config['telefono1_sitio'] = $this->input->post('telefono1_sitio');
            $config['telefono2_sitio'] = $this->input->post('telefono2_sitio');
            //$config['estado_sitio'] = $this->input->post('estado_sitio');
            $config['costo_dominio'] = $this->input->post('costo_dominio');
            $config['costo_dominio_py'] = $this->input->post('costo_dominio_py');
            $config['detalle_dominio'] = $this->input->post('detalle_dominio');
            $config['detalle_dominio_py'] = $this->input->post('detalle_dominio_py');
            $config['plan_reg_dominio'] = $this->input->post('plan_reg_dominio');
            $config['red_social_1'] = $this->input->post('red_social_1');
            $config['red_social_2'] = $this->input->post('red_social_2');
            $config['red_social_3'] = $this->input->post('red_social_3');
			$config['condicion_uso'] = $this->input->post('condicion_uso');
			$config['simbolo_moneda'] = $this->input->post('simbolo_moneda');
			
			$config['portada_texto_1'] = $this->input->post('portada_texto_1');
            $config['portada_texto_2'] = $this->input->post('portada_texto_2');
			$config['portada_texto_dom'] = $this->input->post('portada_texto_dom');
			$config['portada_texto_dom_py'] = $this->input->post('portada_texto_dom_py');
			
            $id = $this->configuracion_model->update($config, $id_config);

            $this->flash_message->success_message('Se ha actualizado la configuración del sitio');
            redirect('adm/portada');
        }

        $data['categorias'] = $this->categoria_plan_model->get_all();
        $data['css_files'] = array('assets/adminlte/plugins/select2/select2.min.css',);
        $data['js_files'] = array('assets/adminlte/plugins/select2/select2.full.min.js', 'assets/adminlte/plugins/ckeditor/ckeditor.js');

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/editar_configuraciones', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function editar_avanzado($id_config = 1)
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('configuraciones_avanzadas')) {
            $config['estado_sitio'] = $this->input->post('estado_sitio');
            $id = $this->configuracion_model->update($config, $id_config);
            $this->flash_message->success_message('Se ha actualizado la configuración del sitio');
            redirect('adm/portada');
        }

        $data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/editar_configuraciones_avanzadas', $data);
        $this->load->view('adm/plantillas/pie');
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Checkear disponibilidad de dominios
 * @property Domain $domain
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Dominios extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function check()
    {
        $data['config'] = $this->config_sitio;
        if ($this->input->method() === 'post') {
            $this->load->library('domain');
            $dominio = $this->input->post('dominio') . $this->input->post('tld');
            $check = $this->domain->is_available($dominio);
            $data['dominio'] = $dominio;
            $data['check'] = $check;
        }

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/check_dominio', $data);
        $this->load->view('plantilla/pie');
    }

    public function check_py()
    {
        $data['config'] = $this->config_sitio;
        if ($this->input->method() === 'post') {
            $this->load->library('domain');
            $dominio = $this->input->post('dominio') . $this->input->post('tld');
            $check = $this->domain->is_available($dominio);
            $data['dominio'] = $dominio;
            $data['check'] = $check;
        }

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/check_py_dominio', $data);
        $this->load->view('plantilla/pie');
    }

    public function registrar()
    {
        if ($this->input->method() !== 'post') {
            redirect(base_url());
        }
        $data['config'] = $this->config_sitio;
        $data['dominio'] = $this->input->post('dominio');

        $this->load->library('form_validation');

        if ($this->input->post('reg_form') && $this->form_validation->run('registrar_dominio')) {
            $registro = $this->input->post();
            $this->load->library('email');

            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $this->email->initialize($config);

            $this->email->from($registro['email'], $registro['nombre'] . ' ' . $registro['apellido']);
            $this->email->to($registro['sitio_email']);

            $this->email->subject('Registro de dominio: ' . $registro['dominio']);
            $mensaje = '<p>Nombre de solicitante: ' . $registro['nombre'] . ' ' . $registro['apellido'] . '</p>';
            $mensaje .= '<p>Cedula: ' . $registro['cedula'] . '</p>';
            $mensaje .= '<p>Email: ' . $registro['email'] . '</p>';
            $mensaje .= '<p>Empresa: ' . $registro['empresa'] . '</p>';
            $mensaje .= '<p>Numero de fax: ' . $registro['fax'] . '</p>';
            $mensaje .= '<p>Numero de telefono: ' . $registro['telefono'] . '</p>';
            $mensaje .= '<p>RUC: ' . $registro['ruc'] . '</p>';
            $mensaje .= '<p>Dirección: ' . $registro['direccion'] . '</p>';
            $mensaje .= '<p>Ciudad: ' . $registro['ciudad'] . '</p>';
            $mensaje .= '<p>Departamento: ' . $registro['departamento'] . '</p>';
            $mensaje .= '<p>Cod. Postal: ' . $registro['cod_postal'] . '</p>';
			$mensaje .= '<p>Plan seleccionado: ' . $registro['planes_hospedaje'] . '</p>';
            $this->email->message($mensaje);
            $enviado = $this->email->send();

            if ($enviado) {
                $this->load->view('plantilla/cabecera', $data);
                $this->load->view('paginas/email_enviado', $data);
                $this->load->view('plantilla/pie');
                return;
            } else {
                $data['error_envio'] = TRUE;
            }
        }

        $this->load->model('planes_model');

        $data['planes'] = $this->planes_model->get_all(array('id_categoria_plan' => $data['config']->plan_reg_dominio, 'habilitado' => TRUE));

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/registrar_dominio', $data);
        $this->load->view('plantilla/pie');
    }
	
	
	public function registrar_py()
    {
        if ($this->input->method() !== 'post') {
            redirect(base_url());
        }
        $data['config'] = $this->config_sitio;
        $data['dominio'] = $this->input->post('dominio');

        $this->load->library('form_validation');

        if ($this->input->post('reg_form') && $this->form_validation->run('registrar_dominio')) {
            $registro = $this->input->post();
            $this->load->library('email');

            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $this->email->initialize($config);

            $this->email->from($registro['email'], $registro['nombre'] . ' ' . $registro['apellido']);
            $this->email->to($registro['sitio_email']);

            $this->email->subject('Registro de dominio: ' . $registro['dominio']);
            $mensaje = '<p>Nombre de solicitante: ' . $registro['nombre'] . ' ' . $registro['apellido'] . '</p>';
            $mensaje .= '<p>Cedula: ' . $registro['cedula'] . '</p>';
            $mensaje .= '<p>Email: ' . $registro['email'] . '</p>';
            $mensaje .= '<p>Empresa: ' . $registro['empresa'] . '</p>';
			$mensaje .= '<p>Rubro: ' . $registro['rubro'] . '</p>';
            $mensaje .= '<p>Numero de fax: ' . $registro['fax'] . '</p>';
            $mensaje .= '<p>Numero de telefono: ' . $registro['telefono'] . '</p>';
            $mensaje .= '<p>RUC: ' . $registro['ruc'] . '</p>';
            $mensaje .= '<p>Dirección: ' . $registro['direccion'] . '</p>';
            $mensaje .= '<p>Ciudad: ' . $registro['ciudad'] . '</p>';
            $mensaje .= '<p>Departamento: ' . $registro['departamento'] . '</p>';
            $mensaje .= '<p>Cod. Postal: ' . $registro['cod_postal'] . '</p>';
			$mensaje .= '<p>Justificacion: ' . $registro['justificacion'] . '</p>';
			$mensaje .= '<p>Plan seleccionado: ' . $registro['planes_hospedaje'] . '</p>';
            $this->email->message($mensaje);
            $enviado = $this->email->send();

            if ($enviado) {
                $this->load->view('plantilla/cabecera', $data);
                $this->load->view('paginas/email_enviado', $data);
                $this->load->view('plantilla/pie');
                return;
            } else {
                $data['error_envio'] = TRUE;
            }
        }

        $this->load->model('planes_model');

        $data['planes'] = $this->planes_model->get_all(array('id_categoria_plan' => $data['config']->plan_reg_dominio, 'habilitado' => TRUE));

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/registrar_dominio_py', $data);
        $this->load->view('plantilla/pie');
    }

}

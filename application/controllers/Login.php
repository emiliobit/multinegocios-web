<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->model('planes_model');
    }

    public function index()
    {
        $data['config'] = $this->configuracion_model->get();
		
		//trae los planes destacados, si los hubiere	
		$data['planes_destacados'] = $this->planes_model->get_all(array('habilitado' => 1, 'destacado' => 1 ));

        //$this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/login', $data);
        //$this->load->view('plantilla/pie');
    }	
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property Item_model $item_model
 */
class Items extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('item_model');
        $this->load->model(array('planes_model', 'categoria_plan_model', 'categoria_item_model','configuracion_model'));

        $this->load->library(array('form_validation', 'flash_message'));
    }

    public function index()
    {
        $data['items'] = $this->item_model->with('categoria_item')->get_all();
		$data['config'] = $this->config_sitio;

        $data['css_files'] = array('assets/adminlte/plugins/datatables/dataTables.bootstrap.css', 'assets/adminlte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
        $data['js_files'] = array('assets/adminlte/plugins/datatables/jquery.dataTables.min.js', 'assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js', 'assets/adminlte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/items', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function agregar_item()
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('item_agregar')) {
            $item['nombre_item'] = $this->input->post('nombre_item');
            $item['detalle'] = $this->input->post('detalle');
            $item['id_categoria_item'] = $this->input->post('id_categoria_item');
            $item['id_categoria_plan'] = $this->input->post('id_categoria_plan');
            $item['habilitado'] = $this->input->post('habilitado');
            $item['destacado'] = $this->input->post('destacado');
            $item['formulario'] = $this->input->post('formulario');
            $id = $this->item_model->insert($item);
            if ($id) {
                $this->flash_message->success_message('Se ha agregado el item con éxito');
                redirect('adm/items');
            } else {
                $this->flash_message->danger_message('Ocurrio un error, no se ha agregado el item');
            }
        }

        $data['css_files'] = array('assets/adminlte/plugins/select2/select2.min.css',);
        $data['js_files'] = array('assets/adminlte/plugins/select2/select2.full.min.js', 'assets/adminlte/plugins/ckeditor/ckeditor.js');

        $data['categorias_plan'] = $this->categoria_plan_model->get_all();
        $data['categorias_item'] = $this->categoria_item_model->get_all();
		$data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/agregar_item', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function editar_item($id_item)
    {
        $data['item'] = $this->item_model->get($id_item);
        if ($this->input->method() === 'post' && $this->form_validation->run('item_editar')) {
            $this->load->helper('validator');
            if (!check_is_unique($this->item_model->table, 'nombre_item', $this->input->post('nombre_item'), $this->item_model->primary_key, $id_item)) {
                $data['form_error']['nombre_item'] = 'Ya exite un item con ese nombre. Intente con otro';
            }
            if (!isset($data['form_error'])) {
                $item['nombre_item'] = $this->input->post('nombre_item');
                $item['detalle'] = $this->input->post('detalle');
                $item['id_categoria_item'] = $this->input->post('id_categoria_item');
                $item['id_categoria_plan'] = $this->input->post('id_categoria_plan');
                $item['habilitado'] = $this->input->post('habilitado');
                $item['destacado'] = $this->input->post('destacado');
                $item['formulario'] = $this->input->post('formulario');
                $id = $this->item_model->update($item, $id_item);
                if ($id) {
                    $this->flash_message->success_message('Se ha editado el item con éxito');
                    redirect('adm/items');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha editado el item');
                }
            }
        }

        $data['categorias_plan'] = $this->categoria_plan_model->get_all();
        $data['categorias_item'] = $this->categoria_item_model->get_all();

        $data['css_files'] = array('assets/adminlte/plugins/select2/select2.min.css',);
        $data['js_files'] = array('assets/adminlte/plugins/select2/select2.full.min.js', 'assets/adminlte/plugins/ckeditor/ckeditor.js');
		$data['config'] = $this->config_sitio;

        $this->load->view('adm/plantillas/cabecera', $data);
        $this->load->view('adm/paginas/editar_item', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function eliminar_item($id_item)
    {
        if ($this->input->method() === 'post') {
            $eliminado = $this->item_model->delete($id_item);
            if ($eliminado) {
                    $this->flash_message->success_message('Se ha eliminado el item con éxito');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha eliminado el item');
                }
            redirect('adm/items');
        }
    }

    public function habilitado_item($id_item, $habilitado)
    {
        if ($habilitado == 0) {
            $item['habilitado'] = 1;
        } else {
            $item['habilitado'] = 0;
        }
        $id = $this->item_model->update($item, $id_item);
        if ($id) {
            if ($item['habilitado'] == 1) {
                $this->flash_message->success_message('Item habilitado');
            } else {
                $this->flash_message->success_message('Item deshabilitado');
            }
        } else {
            $this->flash_message->danger_message('Ocurrio un error. Intente de nuevo');
        }
        redirect('adm/items');
    }

    public function view($id_item, $id_categoria_plan)
    {
        $data['items'] = $this->item_model->get(array('id_item' => $id_item));
        $data['config'] = $this->config_sitio;

        //trae los planes asociados al item, si los hubiere
        $data['planes'] = $this->planes_model->get_all(array('habilitado' => 1, 'id_categoria_plan' => $id_categoria_plan));
		//trae datos del tipo de plan
		$data['datos_planes'] = $this->categoria_plan_model->get_all(array('habilitado' => 1, 'id_categoria_plan' => $id_categoria_plan));
		$data['config'] = $this->config_sitio;

        $this->load->view('plantilla/cabecera', $data);
        $this->load->view('paginas/item', $data);
        $this->load->view('plantilla/pie');
    }

}

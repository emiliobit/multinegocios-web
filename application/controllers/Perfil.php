<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Perfil
 *
 * @property User_model $user_model
 * @property Password_hasher $password_hasher
 * @author Sergio Ivan Alcaraz Cardozo
 */
class Perfil extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'password_hasher', 'flash_message'));
    }

    public function cambiar_password()
    {
        $data = array();
        if ($this->input->method() === 'post' && $this->form_validation->run('cambiar_password')) {
            $pass_actual = $this->input->post('password_actual');
            if ($this->password_hasher->verify($pass_actual, $this->auth->get_user('password'))) {
                $id = $this->auth->get_user('id');
                $user['password'] = $this->input->post('password_nuevo');
                $actualizado = $this->user_model->update($user, $id);
                if ($actualizado) {
                    $this->auth->destroy_all_auto_login();
                    $this->auth->re_login();
                    $this->flash_message->success_message('Contraseña cambiada éxitosamente');
                    redirect('adm');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error al intentar cambiar la contraseña. Intente de nuevo');
                }
            } else {
                $data['error'] = 'La contraseña actual ingresado es incorrecto';
            }
        }

        $this->load->view('adm/plantillas/cabecera');
        $this->load->view('adm/paginas/cambiar_password', $data);
        $this->load->view('adm/plantillas/pie');
    }

    public function editar()
    {
        if ($this->input->method() === 'post' && $this->form_validation->run('editar_perfil')) {
            $password = $this->input->post('password');
            if ($this->password_hasher->verify($password, $this->auth->get_user('password'))) {
                $id = $this->auth->get_user('id');
                $user['name'] = $this->input->post('name');
                $user['last_name'] = $this->input->post('last_name');
                $user['email'] = $this->input->post('email');
                $actualizado = $this->user_model->update($user, $id);
                if ($actualizado) {
                    $this->auth->re_login();
                    $this->flash_message->success_message('Perfil editado éxitosamente');
                    redirect('adm');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error al intentar editar el perfil. Intente de nuevo');
                }
            } else {
                $data['error'] = 'La contraseña ingresado es incorrecto';
            }
        }

        $data['user'] = $this->auth->get_user();
        $this->load->view('adm/plantillas/cabecera');
        $this->load->view('adm/paginas/editar_perfil', $data);
        $this->load->view('adm/plantillas/pie');
    }

}

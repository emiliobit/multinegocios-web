<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Imagenes extends MY_Controller {

	private $upload_config;
    private $upload_messages = array(
        'upload_file_exceeds_limit' => 'El archivo subido excede el tama�o m�ximo permitido: 1MB',
        'upload_invalid_filesize' => 'El archivo subido excede el tama�o m�ximo permitido: 1MB',
        'upload_invalid_dimensions' => 'La imagen excede la dimensi�n maxima permitida 1024x780',
        'upload_invalid_filetype' => 'El tipo de archivo no esta permitido, se admiten jpg, png y gif',
        'upload_no_file_selected' => 'Debe seleccionar una imagen a subir',
    );



    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->upload_config = array(
            'upload_path' => FCPATH . 'imagenes/',
            'allowed_types' => 'jpg|jpeg|png',
            'encrypt_name' => TRUE,
            'max_size' => '1024',
        );
    }

    public function index() 
	{		
		//$data['config'] = $this->config_sitio;
       // $this->load->view('plantilla/cabecera', $data);
       $this->load->view('paginas/administrador_archivos', $data);
       //$this->load->view('plantilla/pie', $data);
    }
	
	public function agregar()
    {
        if ($this->input->method() === 'post') {
            $this->load->library('upload', $this->upload_config);
            if ($this->upload->do_upload('imagen')) {
                /*
				$banner['tipo'] = 0;
                $banner['texto1'] = $this->input->post('texto1');
                $banner['texto2'] = $this->input->post('texto2');
                $banner['texto3'] = $this->input->post('texto3');
                $banner['orden'] = $this->input->post('orden');
                $banner['id_item'] = NULL;
                $banner['habilitado'] = $this->input->post('habilitado');
				*/
                $imagen['imagen'] = $this->upload->data('file_name');

                //$id = $this->banner_model->insert($imagen);
				/*
                if ($id) {
                    $this->flash_message->success_message('Se ha agregado la imagen con �xito');
                    redirect('adm/banners');
                } else {
                    $this->flash_message->danger_message('Ocurrio un error, no se ha agregado la imagen');
                }
				*/
            } else {
                $this->flash_message->danger_message($this->upload->display_errors('', '<br>'));
            }
        }

        $this->load->view('adm/plantillas/cabecera');
        $this->load->view('adm/paginas/agregar_imagen');
        $this->load->view('adm/plantillas/pie');
    }
	
}
